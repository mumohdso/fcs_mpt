import uproot
import pickle
import sklearn
import numpy as np

import shutil
import os

import math
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.font_manager as font_manager

def histplot_withsub_raw(datas, bins, weights=None, usererror = None, labels = None, scale=1., removenorm = None, **kwargs,):
    matplotlib.rcParams['font.sans-serif'] = "FreeSans"
    matplotlib.rcParams['font.family'] = "sans-serif"
    matplotlib.rcParams['ytick.labelsize'] = 15

    settings = {
        "xlabel" : r"$m_{Vh} [GeV]$",
        "ylabel": 'Arbitrary unit',
        "title1": r"$\mathbf{ATLAS}$",# \newline Ptl next-leading, full cuts, 2 b-tags $",
        "title1_1": r"Simulation Internal",
        "title2": r"$\mathit{\sqrt{s}=13\:TeV,36.1\:fb^{-1}}$",# Ptl next-leading, full cuts, 2 b-tags $",
        #"title3": r"$\mathbf{2\;lep.,2\;b-tag}$",
        "title3": "2 lep., 2 b-tag",
        "title4": "2 lep., 2 b-tag",
        "filename": "deltatest2",
        "log_y":True,
        "log_x":False,
        "removenorm":False,
        "central":"fullsim",
        "upper_y": 1.6, 
        "do_errorbar": False
        }
    for each_key in kwargs.items():
        settings[each_key[0]] = kwargs[each_key[0]]
    fig, (ax1, ax2) = plt.subplots(2, 1, gridspec_kw={'height_ratios':[3, 1]}, figsize=(10, 10))
    fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.06)

    if weights is None:
        weights = []
        for each in datas:
            weights.append(np.ones(len(each)))

    if removenorm:
        for i in range(len(weights)):
            totalweightbefore = np.sum(weights[i])
            weights[i] = np.array(weights[i]) / np.sum(weights[i])
            if usererror is not None:
                if usererror[i] is not None:
                    usererror[i] = usererror[i]/totalweightbefore

    sigmas = []
    weight_in_binses = []
    for i in range(len(datas)):
        datas[i] = np.array(datas[i])

        event_location = np.digitize(datas[i]/scale, bins)
        sigma2 = []
        weight_in_bins = []
        for j in range(np.size(bins) - 1):
            bin_weight = weights[i][np.where(event_location == j+1)[0]]
            if not (usererror is not None and usererror[i] is not None):
                sigma2.append(np.sum(bin_weight**2.))
            weight_in_bins.append(np.sum(bin_weight))
            if usererror is not None:
                if usererror[i] is not None:
                    binederror = usererror[i][np.where(event_location == j+1)[0]]
                    sigma2.append(np.sum(binederror**2))

        sigmas.append(np.array(sigma2)**0.5)
        weight_in_binses.append(np.array(weight_in_bins))

    colors = ['k', 'g', 'r', 'c', 'm', 'y']
    # print(np.array(datas).shape, np.array(weights).shape)
    # print(np.array(datas[1]).shape, np.array(weights[1]).shape)
    # print(np.array(datas[0]).shape, np.array(weights[0]).shape)
    for i in range(len(datas)):
        datas[i] = np.array(datas[i])/scale
    ax1.hist(datas, bins, histtype='step', fill=False, color=colors[0:len(datas)], weights=weights)
    bins = np.array(bins)
    bin_centre = []
    for i in range(len(datas)):
        bin_centre = (bins[0:-1] + bins[1:])/2
        ax1.errorbar(bin_centre, weight_in_binses[i], xerr=0.0001, yerr=sigmas[i], fmt='.', color=colors[i], label=str(labels[i]))

    handles, lelabels = ax1.get_legend_handles_labels()
    sys_patch = mpatches.Patch(color='black', hatch='/////', fill=False, linewidth=0, label='fullsim uncertainty')
    handles.append(sys_patch)
    ax1.legend(handles=handles, loc='upper right',prop={'size': 20}, frameon=False)
    
    ymin, ymax = ax1.get_ylim()
    ax1.set_ylim([0,ymax* settings["upper_y"]])
    ax1.text(0.05, 1.55 / 1.7, settings['title1'], fontsize=25, transform=ax1.transAxes, style='italic', fontweight='bold')
    ax1.text(0.227, 1.55/ 1.7, settings['title1_1'], fontsize=25, transform=ax1.transAxes)
    ax1.text(0.05, 1.40 / 1.7, settings['title2'], fontsize=20, transform=ax1.transAxes, style='italic', fontweight='bold')
    ax1.text(0.05, 1.23 / 1.7, settings['title3'], fontsize=18, weight='bold', style='italic', transform=ax1.transAxes)
    ax1.text(0.05, 1.12 / 1.7, settings['title4'], fontsize=18, weight='bold', style='italic', transform=ax1.transAxes)
    ax1.set_ylabel(settings['ylabel'], fontsize=20)
    if settings['log_y']:
        ax1.set_yscale('log')
        ax1.set_ylim([0.1, 10**(math.log10(ymax) * settings["upper_y"])])
        ax1.yaxis.set_major_locator(matplotlib.ticker.LogLocator(base=10,numticks=100))
        ax1.minorticks_on()
    if settings['log_x']:
        ax1.set_xscale('log')
        ax2.set_xscale('log')
    ax1.get_xaxis().set_ticks([])

    # if bins[-1] > 400 and "segment" not in settings["filename"]:
    #     ax1.set_xlim([0, 400])
    #     ax2.set_xlim([0, 400])

    i = -1
    for each_x, each_error, each_y_mc in zip(bin_centre, sigmas[0], weight_in_binses[0]):
        i += 1
        if each_y_mc <= 0:
            continue
        ax2.add_patch(
            matplotlib.patches.Rectangle(
                (each_x - (bins[i+1]-bins[i]) / 2., - each_error/each_y_mc), # x, y
                bins[i+1]-bins[i],        # width
                each_error/each_y_mc*2,        # height
                color='black', alpha=0.5,
                hatch='/////', fill=False, linewidth=0,
                ))


    i = 0
    centerheight = []
    for each_height, each_label in zip(weight_in_binses, labels):
        if i == 0:
            centerheight = np.array(each_height)
            i += 1
            continue
        # for j in range(len(centerheight)):
        #     if centerheight[j] <= 0:
        #         centerheight[j] = 0
        # new_each_height = np.array(each_height)/centerheight
        new_each_height = []
        for eachn, eachd in zip(each_height, centerheight):
            if eachn == 0:
                new_each_height.append(0)
            elif eachd <= 0:
                new_each_height.append(10)
            else:
                new_each_height.append(eachn/eachd)
        new_each_height  = np.array(new_each_height)
        if i != 0:
            new_each_height[np.isnan(new_each_height)] = -10
            new_each_height[np.isposinf(new_each_height)] = 2
            new_each_height[np.isneginf(new_each_height)] = -2
        else:
            new_each_height[np.isnan(new_each_height)] = 1
            new_each_height[np.isinf(new_each_height)] = 1
        ax2.hist(bin_centre, bins, weights=new_each_height-1, label=each_label, histtype=u'step', color=colors[i])
        i += 1
    ax2.set_ylim([-1, 1])
    ax2.plot([bins[0], bins[np.size(bins)-1]], [0, 0], linestyle='--', color='k')
    ax2.set_ylabel("fastsim/" + settings["central"] + "-1", fontsize=20)
    ax2.set_xlabel(settings['xlabel'], fontsize=20)

    fig.savefig(settings['filename'] + '.pdf', bbox_inches='tight', pad_inches = 0.25)
    fig.savefig(settings['filename'] + '.png', bbox_inches='tight', pad_inches = 0.25)
    plt.close(fig)

def pickleit(obj, path):
    outfile = open(path, 'wb')
    pickle.dump(obj, outfile)
    outfile.close()

def unpickleit(path):
    infile = open(path, 'rb')
    output = pickle.load(infile)
    infile.close()
    return output

def loadpcahist(pdg, etamin, etamax, energy, filename):
    allpcas = []
    rootfile = uproot.open(filename)
    histname = "E" + str(energy) +  "_etaMin" + str(etamin) + "_etaMax" + str(etamax)
    for i in range(5):
        dirname = "PCA" + str(i) + "_PDG" + str(pdg)
        if dirname in rootfile:
            if histname in rootfile[dirname]:
                hist = rootfile[dirname][histname].to_numpy()
                allpcas.append([hist[1], hist[0][(hist[0] < 1)]])
            else:
                print("Info: cannot find " + histname + " in " + dirname)
                return 0
        else:
            print("Warning, cannot find " + dirname + " in " + filename)
            return 0
    return allpcas

def runtoy(pcahists, pcaobj, cdfobj, nevents):
    allpcs = []
    for each in pcahists:
        randomarray = np.random.uniform(0, 1, nevents)
        allpcs.append(each[0][np.digitize(randomarray, each[1])])
    return cdfobj.inverse_transform(pcaobj.inverse_transform(np.transpose(allpcs)))

def runtoyall(pcaobj, cdfobj, nevents):
    allpcs = []
    for i in range(5):
        allpcs.append(np.random.normal(0, 1, nevents))
    return cdfobj.inverse_transform(pcaobj.inverse_transform(np.transpose(allpcs)))

def getsimulation(df, eta, energy, pid):
    df = df[np.logical_and(np.logical_and(df["AuxEtaMin"] == eta, df["aux_input_momentum"] == energy), df["kinBranch_pdgID"] == pid)]
    df = df.drop(['aux_input_momentum', 'AuxEtaMin', 'kinBranch_pdgID'], axis=1)
    return df

def getsimulationall(df):
    df = df.drop(['aux_input_momentum', 'AuxEtaMin', 'kinBranch_pdgID'], axis=1)
    return df

def trim(data, maxvalue):
    data = np.array(data)
    data[data>maxvalue] = maxvalue-0.00001
    return data

def makeplot(df_simulation, toy, etamin, etamax, energy, pid):
    i = 0
    for each_column in df_simulation.columns:
        datas = [df_simulation[each_column].to_numpy(), toy[i]]

        joineddata = np.concatenate((df_simulation[each_column].to_numpy(), toy[i]), axis=None)

        bins = np.linspace(min(joineddata), max(joineddata), 30)

        if max(joineddata) > 7000:
            bins = np.linspace(0, 7000, 30)
            # trimming overflowing events
            datas[0] = trim(datas[0], 7000)
            datas[1] = trim(datas[1], 7000)
            # print(max(datas[0]), max(datas[1]))
        

        axname = each_column.replace("kinBranch_", "")
        if "energy" in axname:
            axname += " MeV"

        title2 = str(etamin) + "< eta <" + str(etamax) + ", E = " + str(energy) + " MeV, pid " + str(pid)
        filename= "E_" + str(energy) + "_etamin" + str(etamin) + "_etamax" + str(etamax) +"_pid" + str(pid) + "_" + each_column
        i += 1
        histplot_withsub_raw(datas, bins, labels = ["fullsim", "Toy"], scale=1., removenorm = True, log_y=False, title2=title2, title3="", title4="", filename="output/" + each_column + "/" + filename, xlabel=axname)
        # exit(1)

def runAll(df, energy, etamin, etamax, pdgId, pcahists, pcaobj, cdfobj, nevents = 3000):
    toydata = np.transpose(runtoy(pcahists, pcaobj, cdfobj, nevents))
    df_tem = getsimulation(df, int(etamin), energy, pdgId)
    makeplot(df_tem, toydata, etamin, etamax, energy, pdgId)    

def main():
    #pcaobj = unpickleit("PCA_transform.pkl")
    #cdfobj = unpickleit("CDF_transform.pkl")
    df = unpickleit("df.pkl")
    df['kinBranch_pdgID'] = df['kinBranch_pdgID'].abs()

    print(df)
    pdg_id = [11, 22, 13, 211, 2212, 2112, 321, 310, 130]
    pdg_id_np = [2212, 2112]
    pdg_id_non_np = [11, 13, 22, 211, 321, 310, 130]    
    energy = [16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304]
    eta = ["000", "040", "080", "120", "160", "200", "240", "280", "320"]

    pca_pkl_np_noncrack_path = "../muonpunchthroughparameterization/kinematics_PCA/PCA_transform_pn-noncrack_v00111_6.pkl"
    pca_pkl_np_crack_path = "../muonpunchthroughparameterization/kinematics_PCA/PCA_transform_pn-crack_v00111_6.pkl"  
    pca_pkl_non_np_noncrack_path = "../muonpunchthroughparameterization/kinematics_PCA/PCA_transform_non-pn-noncrack_v00111_6.pkl" 
    pca_pkl_non_np_crack_path = "../muonpunchthroughparameterization/kinematics_PCA/PCA_transform_non-pn-crack_v00111_6.pkl"
    cdf_pkl_np_noncrack_path = "../muonpunchthroughparameterization/kinematics_PCA/CDF_transform_pn-noncrack_v00111_6.pkl" 
    cdf_pkl_np_crack_path = "../muonpunchthroughparameterization/kinematics_PCA/CDF_transform_pn-crack_v00111_6.pkl"  
    cdf_pkl_non_np_noncrack_path = "../muonpunchthroughparameterization/kinematics_PCA/CDF_transform_non-pn-noncrack_v00111_6.pkl"
    cdf_pkl_non_np_crack_path = "../muonpunchthroughparameterization/kinematics_PCA/CDF_transform_non-pn-crack_v00111_6.pkl"

    pca_pkl_np_crack = unpickleit(pca_pkl_np_crack_path)
    cdf_pkl_np_crack = unpickleit(cdf_pkl_np_crack_path)                      
    pca_pkl_np_noncrack = unpickleit(pca_pkl_np_noncrack_path)
    cdf_pkl_np_noncrack = unpickleit(cdf_pkl_np_noncrack_path)                            
    pca_pkl_non_np_crack = unpickleit(pca_pkl_non_np_crack_path)
    cdf_pkl_non_np_crack = unpickleit(cdf_pkl_non_np_crack_path)                            
    pca_pkl_non_np_noncrack = unpickleit(pca_pkl_non_np_noncrack_path)
    cdf_pkl_non_np_noncrack = unpickleit(cdf_pkl_non_np_noncrack_path) 

    param_file_name = "../muonpunchthroughparameterization/param_file_creation/TFCSparam_mpt_test_ok.root"

    shutil.rmtree("output", ignore_errors=True)
    os.mkdir("output")
    for eachc in df.columns:
        if eachc in ['aux_input_momentum', 'AuxEtaMin', 'kinBranch_pdgID']:
            continue
        os.mkdir(os.path.join("output", eachc))


    for eachid in pdg_id:
        for each_energy in energy:
            for i in range(len(eta)):
                if i == len(eta) - 2:
                    break
                print("processing pid = " + str(eachid) + " energy = " + str(each_energy) + ", eta = " + str(eta[i]))
                etamin = eta[i]
                etamax = eta[i+1]
                pcahists = loadpcahist(eachid, etamin, etamax, each_energy, param_file_name)
                if pcahists:
                    if (eachid in pdg_id_np):
                        if(float(etamin) == 120): 
                            pcaobj = pca_pkl_np_crack
                            cdfobj = cdf_pkl_np_crack                      
                        else:
                            pcaobj = pca_pkl_np_noncrack
                            cdfobj = cdf_pkl_np_noncrack                           
                    elif (eachid in pdg_id_non_np):
                        if(float(etamin) == 120): 
                            pcaobj = pca_pkl_non_np_crack
                            cdfobj = cdf_pkl_non_np_crack                            
                        else:
                            pcaobj = pca_pkl_non_np_noncrack
                            cdfobj = cdf_pkl_non_np_noncrack                        
                    else:
                        continue
                    runAll(df, each_energy, etamin, etamax, eachid, pcahists, pcaobj, cdfobj, 3000)

    # plot all samples
    toydata = np.transpose(runtoyall(pcaobj, cdfobj, 30000))
    df_tem = getsimulationall(df)
    makeplot(df_tem, toydata, "0", "320", "all", "all")


if __name__ == "__main__":
    main()
