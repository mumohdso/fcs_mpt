# Muon Punch-Through Parameterization

Code to create muon punch-through parameterization for FCS.

**FCSNTUP_analysis** analyses the raw FCSNTUP files and creates root files each specific energy and eta slice with the required data to create the param file.

**kinematics_PCA** applies a PCA rotation the kinematics of the punch through particles, needed for param file creation. Outputs XML configs to invert the transforms at simulation time.   

**param_file_creation** contains the script used to create a muon punch through param file for FCS.

# Detailed steps can be found here: https://docs.google.com/document/d/155UuIPtRvUr-77Ln5VK475wasjmrQyU4nlfgI-lRDao/edit?usp=sharing

