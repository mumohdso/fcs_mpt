def print_to_file(item, filename):
    with open(filename, 'w') as f:
        print(item, file=f)

def print_inverseCDF_xml_combine(quantiles_arr, references_arr, pca_type_arr, filename):
    with open(filename, 'w') as f:

        print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", file=f)
        print("<CDFMappings>", file=f)
        #loop here
        for i, item in enumerate(pca_type_arr):
            print("\t<pca_type-" + item + ">", file=f)
            print_inverseCDF_xml_body(quantiles_arr[i],references_arr[i],f)
            print("\t</pca_type-" + item + ">", file=f)
        print("</CDFMappings>", file=f)

def print_inversePCA_xml_combine(pca_components_arr, means_arr, pca_type_arr, filename):    
    with open(filename, 'w') as f:

        print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", file=f)
        print("<PCAinverse>", file=f)
        #loop here
        for i, item in enumerate(pca_type_arr):
            print("\t<pca_type-" + item + ">", file=f)
            print_inversePCA_xml_body(pca_components_arr[i],means_arr[i],f)        
            print("\t</pca_type-" + item + ">", file=f)
        print("</PCAinverse>", file=f)


def print_inverseCDF_xml_body(quantiles, references, fileobj):    
    for j in range(quantiles.shape[1]):
        print("\t\t<variable" + str(j) + ">", file=fileobj)

        for i in range(quantiles.shape[0]):
            print("\t\t\t<CDFmap ref=\"" + str(references[i]) + "\" quant=\"" + str(quantiles[i, j]) + "\"/>", file=fileobj)
        
        print("\t\t</variable" + str(j) + ">", file=fileobj)


def print_inversePCA_xml_body(pca_components, means, fileobj):    
    for i in range(pca_components.shape[0]):
        print("\t\t<PCAmatrix comp_0=\"" + str(pca_components[i, 0]) +"\" comp_1=\"" + str(pca_components[i, 1]) +"\" comp_2=\"" + str(pca_components[i, 2]) +"\" comp_3=\"" + str(pca_components[i, 3]) +"\" comp_4=\"" + str(pca_components[i, 4]) + "\"/>", file=fileobj)        
    print("\t\t<PCAmeans mean_0=\"" + str(means[0]) +"\" mean_1=\"" + str(means[1]) +"\" mean_2=\"" + str(means[2]) +"\" mean_3=\"" + str(means[3]) +"\" mean_4=\"" + str(means[4]) + "\"/>", file=fileobj)


def print_inverseCDF_xml(quantiles, references, filename):
    
    with open(filename, 'w') as f:

        print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", file=f)
        print("<CDFMappings>", file=f)
        
        for j in range(quantiles.shape[1]):
            print("\t<variable" + str(j) + ">", file=f)

            for i in range(quantiles.shape[0]):
                print("\t\t<CDFmap ref=\"" + str(references[i]) + "\" quant=\"" + str(quantiles[i, j]) + "\"/>", file=f)
            
            print("\t</variable" + str(j) + ">", file=f)


        print("</CDFMappings>", file=f)


def print_inversePCA_xml(pca_components, means, filename):
    
    with open(filename, 'w') as f:

        print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", file=f)
        print("<PCAinverse>", file=f)
        
        for i in range(pca_components.shape[0]):
            print("\t<PCAmatrix comp_0=\"" + str(pca_components[i, 0]) +"\" comp_1=\"" + str(pca_components[i, 1]) +"\" comp_2=\"" + str(pca_components[i, 2]) +"\" comp_3=\"" + str(pca_components[i, 3]) +"\" comp_4=\"" + str(pca_components[i, 4]) + "\"/>", file=f)
            
        print("\t<PCAmeans mean_0=\"" + str(means[0]) +"\" mean_1=\"" + str(means[1]) +"\" mean_2=\"" + str(means[2]) +"\" mean_3=\"" + str(means[3]) +"\" mean_4=\"" + str(means[4]) + "\"/>", file=f)

        print("</PCAinverse>", file=f)
