import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.transforms as trns
from matplotlib.colors import LogNorm

from scipy import stats

import sys
sys.path.append('/eos/user/t/thcarter/python_packages/venv/lib/python3.6/site-packages/') #path containing atlasify
from atlasify import atlasify
from atlasify import monkeypatch_axis_labels
monkeypatch_axis_labels()

def corrfunc(x, y, **kws):
    r, _ = stats.pearsonr(x, y)
    ax = plt.gca()
    # count how many annotations are already present
    n = len([c for c in ax.get_children() if 
                  isinstance(c, matplotlib.text.Annotation)])
    pos = (.07, .87 - .1*n)
    # or make positions for every label by hand
    #pos = (.1, .9) if kws['label'] == 'Yes' else (.1,.8)

    ax.annotate("{}: {:.2f}".format(kws['label'],r),
                xy=pos, xycoords=ax.transAxes, fontsize=13)

def pairgrid_heatmap(x, y, **kws):
    cmap = sns.light_palette(kws.pop("color"), as_cmap=True)
    plt.hist2d(x, y, cmap=cmap, cmin=1, **kws)


def plot_pairs(df, saveLabel, isGauss=False, atlasLabel='Simulation Internal', frac=0.5):   
    df_sample = df.sample(frac=frac)

    g = sns.PairGrid(df_sample)
    g.map_diag(plt.hist, bins=50, alpha=0.75)

    cmap = sns.light_palette('C0', as_cmap=True)



    g.map_offdiag(pairgrid_heatmap, bins=50, label='correlation')

    g.map_lower(corrfunc, label='correlation')
    
    #cbar.solids.set_edgecolor("face")

    for i, ax in enumerate(g.axes.flat):
    
        if(i < 1):
            atlasify(atlasLabel, r"$\pi^{\pm}$, 64 GeV - 4.2 TeV, $\eta < 3.2$", outside=True, enlarge=10, axes=ax)
    
    
        atlasify(brand='', axes=ax)
    
        ax.spines['top'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        
        if (isGauss):
            ax.set_ylim(-5,5)
            ax.set_xlim(-5,5)
        ax.xaxis.label.set_size(14)
        ax.yaxis.label.set_size(14)
        
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(13)
                
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(13)
        
    cax = g.fig.add_axes([g.axes.flat[4].get_position().x1+0.03,g.axes.flat[24].get_position().y0,0.03,g.axes.flat[4].get_position().y1-g.axes.flat[24].get_position().y0])
    cbar = plt.colorbar( cax=cax)
    cbar.set_label('Number of events', rotation=90, size=14)
    #plt.colorbar(ax=g.axes.ravel().tolist())
    cbar.ax.tick_params(labelsize=13) 
    
    plt.savefig(saveLabel + ".pdf", bbox_inches = "tight")
