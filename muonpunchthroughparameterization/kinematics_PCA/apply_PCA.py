import ROOT
import uproot
import pandas as pd
import numpy as np
import argparse
import logging

import pickle

from sklearn.preprocessing import QuantileTransformer
from sklearn.decomposition import PCA

#local imports
import plotting
import create_xml_configs

# set logging level to INFO
logging.basicConfig(level = logging.INFO)

# commandline arguments
parser = argparse.ArgumentParser(description="Run PCA",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-ntupPath",'--ntupPath',type=str,
                    help="ntupPath", required=True)     
parser.add_argument("-paramVer",'--paramVer',type=str,
                    help="paramVer", required=True)     
args = parser.parse_args()
config = vars(args)
logging.info("Parsing command line arguments")
logging.info("Args: " + str(args))
logging.info(args.ntupPath); #ntupPath = "../FCSNTUP_analysis/<condor_output>/pid211/*/*/*.root:kinematicsTree"

def pickleit(obj, path):
    outfile = open(path, 'wb')
    pickle.dump(obj, outfile)
    outfile.close()

def createPCA(data_df, pca_type = "", ver = "v02"):
    data_df_noAux = data_df.drop(['aux_input_momentum', 'AuxEtaMin', 'kinBranch_pdgID'], axis=1) #drop aux variables

    #initialise scaler and apply, CDF transform to Gaussian
    print('applying CDF transform for: ' + pca_type)
    scaler = QuantileTransformer(output_distribution='normal')
    data_df_noAux[data_df_noAux.columns] = scaler.fit_transform(data_df_noAux[data_df_noAux.columns])

    #save CDF transform
    print('saving CDF_transform.pkl for: ' + pca_type)
    pickleit(scaler,'CDF_transform_' + pca_type + '_' + ver + '.pkl')

    #rename df columns for nice plotting
    data_df_noAux = data_df_noAux.rename(columns={"kinBranch_energy": "Energy transformed \n to gaussian",
                              "kinBranch_deflectionAngle_theta": "$\Delta\\theta$ transformed \n to gaussian",
                              "kinBranch_deflectionAngle_phi":"$\Delta\phi$ transformed \n to gaussian",
                              "kinBranch_momDeflectionAngle_theta":"$\Delta\\theta_p$ transformed \n to gaussian",
                              "kinBranch_momDeflectionAngle_phi":"$\Delta\phi_p$ transformed \n to gaussian"})

    #plot kinematics after transform to gaussian but before PCA
    print('plotting for: ' + pca_type)
    plotting.plot_pairs(data_df_noAux, 'kinematics_pairplot_' + pca_type + '_beforePCA', isGauss=True)

    #apply PCA
    print('applying PCA transform for: ' + pca_type)
    pca = PCA(n_components=5)
    principalComponents = pca.fit_transform(data_df_noAux)
    principalDf = pd.DataFrame(data = principalComponents, columns = ['Principal component 0', 'Principal component 1', 'Principal component 2', 'Principal component 3', 'Principal component 4'])

    print('explained variance %: for ' + pca_type)
    print(pca.explained_variance_ratio_, sep = ", ")
    #plot kinematics after PCA
    print('plotting for: ' + pca_type)
    plotting.plot_pairs(principalDf, 'kinematics_pairplot_' + pca_type + '_afterPCA', isGauss=True)

    # save PCA.pkl for use in param_file_creation
    print('saving PCA_transform.pkl for: ' + pca_type)
    pickleit(pca,'PCA_transform_' + pca_type + '_' + ver + '.pkl')

    return scaler, pca, pca_type, ver

def savePCA_All(scaler_arr, pca_arr, pca_type_arr,ver):
    quantiles_arr = []
    references_arr = []
    pca_components_arr = []
    means_arr = []
    for i, item in enumerate(scaler_arr):
        quantiles_arr.append(item.quantiles_)
        references_arr.append(item.references_)
    for i, item in enumerate(pca_arr):
        pca_components_arr.append(item.components_.T)
        means_arr.append(item.mean_)
    print('writing xml configs for all')
    create_xml_configs.print_inverseCDF_xml_combine(quantiles_arr, references_arr, pca_type_arr, "TFCSparam_mpt_inverseCdf_" + ver + ".xml")  
    create_xml_configs.print_inversePCA_xml_combine(pca_components_arr, means_arr, pca_type_arr, "TFCSparam_mpt_inversePca_" + ver + ".xml")

def savePCA_Single(scaler,pca,pca_type,ver):  
    print('writing xml configs for: ' + pca_type)
    #write inverse CDF transform xml
    create_xml_configs.print_inverseCDF_xml(scaler.quantiles_, scaler.references_, "TFCSparam_mpt_inverseCdf_" + pca_type + "_" + ver + ".xml")
    #write inverse PCA transform xml
    create_xml_configs.print_inversePCA_xml(pca.components_.T, pca.mean_, "TFCSparam_mpt_inversePca_" + pca_type + "_" + ver + ".xml")
    #sanity check, print raw PCA
    print('writing txt prints for: ' + pca_type)    
    create_xml_configs.print_to_file(pca.mean_,"rawMeans_" + pca_type + ".txt")
    create_xml_configs.print_to_file(pca.components_.T,"rawPCA_" + pca_type + ".txt")

def main(ntupPath = ""):
    pdgs_to_consider_nonpn = [11, -11, 13, -13, 22, -22, 211, -211, 321, -321, 310, 130]
    pdgs_to_consider_pn = [2212, -2212, 2112]
    pdgs_to_consider = [11, -11, 13, -13, 22, -22, 211, -211, 2212, -2212, 2112, 321, -321, 310, 130]

    # load input data, from two branches 
    print('loading input data')    
    data_df = uproot.concatenate(ntupPath + ':kinematicsTree', library="pd", allow_missing=True)
    #data_df = uproot.concatenate(ntupPath + ':kinematicsTree', library="pd", allow_missing=True)
    #data_df_auxtree = uproot.concatenate(ntupPath + ':AuxTree', library="pd", allow_missing=True)

    # combine the two dataframes
    data_df = data_df.drop(['eventNumber'], axis=1)
    
    # dump to a file
    #pickleit(data_df,'df.pkl')        

    # crack region from 132 to 157 but take 120 to 160 to model here
    crack_min_eta = 120
    crack_max_eta = 160

    data_df['AuxEtaMin'] = data_df['AuxEtaMin'].astype('float64')
    # DEBUG: check (TODO: add debug bool)
    print("data_df:\n")
    print(data_df)

    #TODO: test, remove this
    data_df['kinBranch_energy'] = data_df['kinBranch_energy'] - 1000

    data_df_eta_crack = data_df.query('AuxEtaMin >= 120 & AuxEtaMin < 160')
    data_df_eta_noncrack = data_df.query('AuxEtaMin < 120 | AuxEtaMin >= 160')

    # DEBUG: check (TODO: add debug bool)
    print("data_df_eta_noncrack:\n")
    print(data_df_eta_noncrack)
    print("data_df_eta_crack:\n")
    print(data_df_eta_crack)

    # crack region
    data_df_eta_crack = data_df_eta_crack.loc[data_df_eta_crack['kinBranch_pdgID'].isin(pdgs_to_consider)]
    #non-crack region
    data_df_eta_noncrack = data_df_eta_noncrack.loc[data_df_eta_noncrack['kinBranch_pdgID'].isin(pdgs_to_consider)]

    # split by pdgId groups
    # proton/neutron
    data_df_pn_crack = data_df_eta_crack.loc[data_df_eta_crack['kinBranch_pdgID'].isin(pdgs_to_consider_pn)]
    data_df_pn_noncrack = data_df_eta_noncrack.loc[data_df_eta_noncrack['kinBranch_pdgID'].isin(pdgs_to_consider_pn)]
    # non proton/neutron
    data_df_nonpn_crack = data_df_eta_crack.loc[data_df_eta_crack['kinBranch_pdgID'].isin(pdgs_to_consider_nonpn)]
    data_df_nonpn_noncrack = data_df_eta_noncrack.loc[data_df_eta_noncrack['kinBranch_pdgID'].isin(pdgs_to_consider_nonpn)]

    # take only absolute values of the pdgId
    # proton/neutron
    data_df_pn_crack['kinBranch_pdgID'] = data_df_pn_crack['kinBranch_pdgID'].abs() #use absolute pdgs
    data_df_pn_noncrack['kinBranch_pdgID'] = data_df_pn_noncrack['kinBranch_pdgID'].abs() #use absolute pdgs
    # non proton/neutron
    data_df_nonpn_crack['kinBranch_pdgID'] = data_df_nonpn_crack['kinBranch_pdgID'].abs() #use absolute pdgs
    data_df_nonpn_noncrack['kinBranch_pdgID'] = data_df_nonpn_noncrack['kinBranch_pdgID'].abs() #use absolute pdgs

    # DEBUG: check (TODO: add debug bool)
    print("data_df_pn_crack:\n")
    print(data_df_pn_crack)
    print("data_df_pn_noncrack:\n")
    print(data_df_pn_noncrack)
    print("data_df_nonpn_crack:\n")
    print(data_df_nonpn_crack)
    print("data_df_nonpn_noncrack:\n")
    print(data_df_nonpn_noncrack)

    # DEBUG: check (TODO: add debug bool)
    print("data_df_eta_crack:\n")
    print(data_df_eta_crack)
    print("data_df_eta_noncrack:\n")
    print(data_df_eta_noncrack)

    # initialize
    scaler_arr =[] 
    pca_arr =[] 
    pca_type_arr =[]
    # data to enter PCA
    data_df_arr =[("pn-crack",data_df_pn_crack), \
                  ("pn-noncrack",data_df_pn_noncrack), \
                  ("non-pn-crack",data_df_nonpn_crack), \
                  ("non-pn-noncrack",data_df_nonpn_noncrack) \
                  ]

    #loop the PCA xml
    for i, item in enumerate(data_df_arr):
        (scaler, pca, pca_type, ver) = createPCA(item[1], item[0], args.paramVer)
        scaler_arr.append(scaler)
        pca_arr.append(pca)
        pca_type_arr.append(pca_type)
        savePCA_Single(scaler,pca,pca_type, ver)
    #all
    savePCA_All(scaler_arr, pca_arr, pca_type_arr, ver)

if __name__ == "__main__":
    main(args.ntupPath)
