import os
import argparse
import logging
import numpy as np
import subprocess
import ROOT
from pathlib import Path

import pandas as pd
import re
import uproot
import pickle

import sklearn
from sklearn.decomposition import PCA
from sklearn.preprocessing import QuantileTransformer

# set logging level to INFO
logging.basicConfig(level = logging.INFO)

# commandline arguments
parser = argparse.ArgumentParser(description="Run Param File",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-ntupPath",'--ntupPath',type=str,
                    help="ntupPath", required=True)   
parser.add_argument("-paramVer",'--paramVer',type=str,
                    help="paramVer", required=True)                           
args = parser.parse_args()
config = vars(args)
logging.info("Parsing command line arguments")
logging.info("Args: " + str(args))
logging.info(args.ntupPath); #ntupPath = "../FCSNTUP_analysis/<condor_output>/pid211/*/*/*.root:kinematicsTree"

def unpickleit(path):
    infile = open(path, 'rb')
    output = pickle.load(infile)
    infile.close()
    return output

def main():
    energies = [16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304] #energies to be used in param
    pdgIDs_np = [2212, 2112]
    pdgIDs_non_np = [11, 13, 22, 211, 321, 310, 130]
    pdgIDs = [11, 22, 13, 211, 2212, 2112, 321, 310, 130] #PDGIDs of punch through particles used in param
    histNames = ["FREQ", 'PCA0', 'PCA1', 'PCA2', 'PCA3', 'PCA4'] #hists to use in param file

    nEtasToMerge = 8 #supply level of merging in eta (1 = no merging)
    maxEta = 320 #max eta to consider in param (default 320)

    #point to directory head of FCSNTUP_analysis condor output
    #dirHead = "../FCSNTUP_analysis/<condor_output>/pid211/"
    dirHead = args.ntupPath

    # TODO: 4 pkl files here, cdf_pkl_path_arr, pca_pkl_path_arr
    #path to PCA_transform.pkl and CDF_transform.pkl files from kinematics_PCA
    #cdf_pkl_path = "../kinematics_PCA/CDF_transform.pkl"
    #pca_pkl_path = "../kinematics_PCA/PCA_transform.pkl"

    pca_pkl_np_noncrack_path = "../kinematics_PCA/PCA_transform_pn-noncrack_v00111_6.pkl"
    pca_pkl_np_crack_path = "../kinematics_PCA/PCA_transform_pn-crack_v00111_6.pkl"  
    pca_pkl_non_np_noncrack_path = "../kinematics_PCA/PCA_transform_non-pn-noncrack_v00111_6.pkl" 
    pca_pkl_non_np_crack_path = "../kinematics_PCA/PCA_transform_non-pn-crack_v00111_6.pkl"
    cdf_pkl_np_noncrack_path = "../kinematics_PCA/CDF_transform_pn-noncrack_v00111_6.pkl" 
    cdf_pkl_np_crack_path = "../kinematics_PCA/CDF_transform_pn-crack_v00111_6.pkl"  
    cdf_pkl_non_np_noncrack_path = "../kinematics_PCA/CDF_transform_non-pn-noncrack_v00111_6.pkl"
    cdf_pkl_non_np_crack_path = "../kinematics_PCA/CDF_transform_non-pn-crack_v00111_6.pkl"

    pca_pkl_np_crack = unpickleit(pca_pkl_np_crack_path)
    cdf_pkl_np_crack = unpickleit(cdf_pkl_np_crack_path)                      
    pca_pkl_np_noncrack = unpickleit(pca_pkl_np_noncrack_path)
    cdf_pkl_np_noncrack = unpickleit(cdf_pkl_np_noncrack_path)                            
    pca_pkl_non_np_crack = unpickleit(pca_pkl_non_np_crack_path)
    cdf_pkl_non_np_crack = unpickleit(cdf_pkl_non_np_crack_path)                            
    pca_pkl_non_np_noncrack = unpickleit(pca_pkl_non_np_noncrack_path)
    cdf_pkl_non_np_noncrack = unpickleit(cdf_pkl_non_np_noncrack_path) 

    #name of output param file
    param_file_name = "TFCSparam_mpt_test_ok.root"

    #configs are set up, now create param file!

    #remove ./tmp directory from previous runs
    subprocess.run(("rm -r ./tmp").split()) #remove ./tmp directory

    #merge input files based on nEtasToMerge using ROOT hadd, saves in ./tmp directory
    merge_input_files(dirHead, energies, nEtasToMerge, maxEta)

    #create structure of param file based on histNames and PDGIDs
    param_file = create_param_file(histNames, pdgIDs, param_file_name)

    # TODO: check here by split two groups of pdgId
    # TODO: check here by split two groups of eta

    #fill the param file with histograms containing the CDF for each punch through kinematic
    #pn
    fill_param_file(param_file, pca_pkl_np_noncrack, cdf_pkl_np_noncrack, pdgIDs_np, nEtasToMerge, (0, 120))
    fill_param_file(param_file, pca_pkl_np_crack, cdf_pkl_np_crack, pdgIDs_np, nEtasToMerge, (120, 160))
    fill_param_file(param_file, pca_pkl_np_noncrack, cdf_pkl_np_noncrack, pdgIDs_np, nEtasToMerge, (160, maxEta))
    #non-pn
    fill_param_file(param_file, pca_pkl_non_np_noncrack, cdf_pkl_non_np_noncrack, pdgIDs_non_np, nEtasToMerge, (0, 120))
    fill_param_file(param_file, pca_pkl_non_np_crack, cdf_pkl_non_np_crack, pdgIDs_non_np, nEtasToMerge, (120, 160))
    fill_param_file(param_file, pca_pkl_non_np_noncrack, cdf_pkl_non_np_noncrack, pdgIDs_non_np, nEtasToMerge, (160, maxEta))

    #finally delete ./tmp
    subprocess.run(("rm -r ./tmp").split()) #remove ./tmp directory

def merge_input_files(dirHead, energies, nEtasToMerge, maxEta):
    Path("./tmp").mkdir(parents=True, exist_ok=True)
    etas = np.arange(0, maxEta, 5).tolist()
    for energy in energies:
        for etaMergeIdx in np.arange(0, len(etas), nEtasToMerge).tolist():
            filesList = []

            mergeCommand = "hadd -f ./tmp/E"
            for etaMin in etas[etaMergeIdx : etaMergeIdx + nEtasToMerge]:
                etaString = "m" + str(etaMin + 5) + "_m" + str(etaMin) + "_" + str(etaMin) + "_" + str(etaMin + 5)
                filePath = dirHead + str(energy) + "/" + etaString + "/"
                fileName = "param_E" + str(energy) + "_" + etaString + ".root"

                if os.path.exists(filePath + fileName):
                    filesList.append(filePath + fileName)
                outEtaMax = str(etaMin + 5)

            mergeCommand += str(energy) + "_etaMin" + str(etas[etaMergeIdx]).zfill(3) + "_etaMax" + str(outEtaMax).zfill(3) + ".root " + " ".join(filesList)
            print(mergeCommand)

            subprocess.run(mergeCommand.split()) #run hadd root command to merge files and store them in ./tmp

def create_param_file(histNames, pdgIDs, param_file_name):

    #create the mpt param file and add the directories corresponding to histNames and pdgIDs chosen to save
    outFile = ROOT.TFile(param_file_name, "RECREATE")
    for histName in histNames:
        for pdg in pdgIDs:

            outDir = outFile.mkdir(histName + "_PDG" + str(pdg))

    return outFile

def fill_hist_from_values(values, name, n_bins, x_min, x_max):

    hist = ROOT.TH1F(name, name, n_bins, x_min, x_max)

    for value in values:
        hist.Fill(value)

    return hist

def fill_hist_int_from_values(values, name, n_bins, x_min, x_max):

    hist = ROOT.TH1I(name, name, n_bins, x_min, x_max)

    for value in values:
        hist.Fill(value)

    return hist

def fill_param_file(param_file, pca, scaler, pdgIDs, numMerge, etaRange = (0,320)):

    list_of_directory_names = [x.GetName() for x in param_file.GetListOfKeys()] #list of directories that were created in param_file
    list_of_directory_names

    for filename in os.listdir("./tmp/"):

        if  filename.endswith(".root"):

            #print("DOING: " + filename + " for etaRange = (" + str(etaRange[0]) + ", " + str(etaRange[1]) + ")")
            in_file = ROOT.TFile("./tmp/" + filename, "READ")
            aux_tree = in_file.Get("AuxTree")

            if aux_tree == None:
                continue

            n_events_total = aux_tree.GetEntries()
            in_file.Close()

            #hadd -f ./tmp/E16384_etaMin080_etaMax120.root ../FCSNTUP_analysis/outputs/sub_out_wFlags_hasxAODGhostMuonSegment_1/pid211/16384/m85_m80_80_85/param_E16384_m85_m80_80_85.root ../FCSNTUP_analysis/outputs/sub_out_wFlags_hasxAODGhostMuonSegment_1/pid211/16384/m90_m85_85_90/param_E16384_m90_m85_85_90.root ../FCSNTUP_analysis/outputs/sub_out_wFlags_hasxAODGhostMuonSegment_1/pid211/16384/m95_m90_90_95/param_E16384_m95_m90_90_95.root ../FCSNTUP_analysis/outputs/sub_out_wFlags_hasxAODGhostMuonSegment_1/pid211/16384/m100_m95_95_100/param_E16384_m100_m95_95_100.root ../FCSNTUP_analysis/outputs/sub_out_wFlags_hasxAODGhostMuonSegment_1/pid211/16384/m105_m100_100_105/param_E16384_m105_m100_100_105.root ../FCSNTUP_analysis/outputs/sub_out_wFlags_hasxAODGhostMuonSegment_1/pid211/16384/m110_m105_105_110/param_E16384_m110_m105_105_110.root ../FCSNTUP_analysis/outputs/sub_out_wFlags_hasxAODGhostMuonSegment_1/pid211/16384/m115_m110_110_115/param_E16384_m115_m110_110_115.root ../FCSNTUP_analysis/outputs/sub_out_wFlags_hasxAODGhostMuonSegment_1/pid211/16384/m120_m115_115_120/param_E16384_m120_m115_115_120.root
            kin_data = uproot.concatenate("./tmp/" + filename + ":kinematicsTree", library="pd")

            print(n_events_total)
            if(n_events_total < 1):
                continue

            #https://roottalk.root.cern.narkive.com/jHkic3qH/root-problem-creating-directories-in-tfile
            #https://root-forum.cern.ch/t/accessing-gdirectory-from-pyroot/12662
            #infile.cd()
            #gDirectory.mkdir
            #gDirectory.Get("subfolder")

            energy_etaMin_etaMax = re.findall(r"\d+", filename)

            if (float(energy_etaMin_etaMax[1]) >= etaRange[0]) & (float(energy_etaMin_etaMax[1]) < etaRange[1]) :
                print("FOUND: " + filename + " for etaRange = (" + str(etaRange[0]) + ", " + str(etaRange[1]) + ")")

                hist_name = "E" + energy_etaMin_etaMax[0] + "_etaMin" + energy_etaMin_etaMax[1] + "_etaMax" + energy_etaMin_etaMax[2]

                kin_data = kin_data.drop(['AuxEtaMin', 'aux_input_momentum'], axis=1)

                for pdg in pdgIDs:

                    kin_data_pdg = kin_data[abs(kin_data["kinBranch_pdgID"]) == int(pdg)]
                    kin_data_pdg = kin_data_pdg.drop(['kinBranch_pdgID'], axis=1)

                    #here we do freq and then drop the column
                    n_events_pdg = kin_data_pdg['eventNumber'].nunique()
                    zero_freq_events = [0] * (n_events_total - n_events_pdg)
                    non_zero_freq_events = kin_data_pdg['eventNumber'].value_counts().values

                    #print(kin_data_pdg['eventNumber'].value_counts().values)
                    hist_freq = fill_hist_from_values( [*non_zero_freq_events, *zero_freq_events], hist_name, 50, 0, 50)
                    inDir_freq = param_file.Get("FREQ_PDG" + str(pdg))
                    inDir_freq.cd()
                    hist_freq_cumulative = hist_freq.GetCumulative(True, "")
                    hist_freq_cumulative.Scale(1./hist_freq.Integral())
                    hist_freq_cumulative.Write()
                    del hist_freq_cumulative
                    del hist_freq

                    kin_data_pdg = kin_data_pdg.drop(['eventNumber'], axis=1)

                    if kin_data_pdg.shape[0] == 0:
                        continue

                    #apply scaler and PCA
                    kin_data_pdg[kin_data_pdg.columns] = scaler.transform(kin_data_pdg[kin_data_pdg.columns])
                    kin_data_pdg_PC = pca.transform(kin_data_pdg)
                    kin_data_pdg = pd.DataFrame(data = kin_data_pdg_PC, columns = ['PCA0', 'PCA1', 'PCA2', 'PCA3', 'PCA4'])

                    for column in kin_data_pdg.columns:

                        hist = fill_hist_from_values(kin_data_pdg[column].values, hist_name, 50, -5, 5)
                        hist_cumulative = hist.GetCumulative(True,"")
                        hist_cumulative.Scale(1./hist.Integral())
                        inDir = param_file.Get(column + "_PDG" + str(pdg))

                        inDir.cd()
                        hist_cumulative.Write()
                        del hist_cumulative
                        del hist
            else:
                continue
        else:
            continue

if __name__ == '__main__':
    main()
