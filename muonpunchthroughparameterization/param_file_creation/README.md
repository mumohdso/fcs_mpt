# param_file_creation

Python script to take individual outputs from FCSNTUP_analysis and create the MPT param file

Options to control energies, eta slices, histograms to be stored in param file in create_param_file.py

Can specify eta merging scheme in create_param_file.py

Point create_param_file.py to head path of output from FCSNTUP_analysis

To run:

	python3 create_param_file.py

will create ./tmp file to perform hadd merging, removed on end.

output mpt_param.root
	
