#ifndef FCSNTUPANASKELETON_H_
#define FCSNTUPANASKELETON_H_

#include <fstream>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <sstream>


#include <TSystem.h>

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TStyle.h>
#include <TInterpreter.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TGeoTube.h>
#include <TGeoManager.h>




/*

liza.mijovic@cern.ch

Minimal skeleton for analysing flat ntuples.
See README for instructions.

 */

class FCSNTUPAnaSkeleton {

 public :
  TTree* mTree;
  int mMaxEvents;

  std::string mOutFileName;
  std::string prettySlice;

  TTree* mFlagTree;
  TFile* mFlagFile;
  std::string mFlagFileName;
  std::string mFlag;
  int mFlagBool;

  Int_t flag;
  TBranch *b_flag = 0;


  std::map<long long, Int_t> m_phi_flag_map;

  long long truthPhi;
  TBranch *b_truthPhi = 0;

  long long truthEta;
  TBranch *b_truthEta = 0;

  std::string mGraphTitleInfo;
  std::string mOutDir;
  FCSNTUPAnaSkeleton(const std::string& pInFileName, const std::string& pTreeName, const std::string& pOutFileName, const std::string& pPDGID,  const std::string& pEnergy,  const std::string& pEtaMin, const std::string& pEtaMax, const std::string& pFlagFileName, const std::string& pFlag, const std::string& pFlagBool, const std::string& pInFileList="");
  void fInit(TTree* pTree);
  void fAnalyse(int pNevents=-1);
  void checkHistAndWrite1D(TH1F hist);
  void checkHistAndWrite2D(TH2F hist);

  void setUpBranches(TTree * tree, std::vector<Float_t> &o_data_vec, std::vector<std::string> vFeatures);
  void DivideVectorByScalar(std::vector<Float_t> &v, Float_t k);

  double minPunchThruEnergyCut = 50;
  int energyPoints[11] = {4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304};
  int etaPoints[54];

  std::vector<std::string> vCellBranchNames{"ECell0_div_Etot", "ECell1_div_Etot", "ECell2_div_Etot", "ECell3_div_Etot", "ECell4_div_Etot",
                                            "ECell5_div_Etot", "ECell6_div_Etot", "ECell7_div_Etot", "ECell8_div_Etot", "ECell9_div_Etot",
                                            "ECell10_div_Etot", "ECell11_div_Etot", "ECell12_div_Etot", "ECell13_div_Etot", "ECell14_div_Etot",
                                            "ECell15_div_Etot", "ECell16_div_Etot", "ECell17_div_Etot", "ECell18_div_Etot", "ECell19_div_Etot",
                                            "ECell20_div_Etot", "ECell21_div_Etot", "ECell22_div_Etot", "ECell23_div_Etot"};


  void set_eta_points(){
	  int counter = 0;
	  for (unsigned int i = 0; i < 270; i = i + 5){
		  etaPoints[counter] = i;
		  counter++;
	  }
  }

  void set_eta_bin_values(TAxis * axis){
    for (int bin = 0; bin < axis->GetNbins(); bin++){
      std::stringstream stream;
      stream << std::fixed << std::setprecision(3) << (etaPoints[bin]+2.5)*0.01;
      std::string s = stream.str();
      axis->SetBinLabel(bin+1, s.c_str());
    }
  }

  void set_energy_bin_values(TAxis * axis){
    for (int bin = 0; bin < axis->GetNbins(); bin++){
      axis->SetBinLabel(bin+1, (std::to_string(energyPoints[bin])).c_str());
    }
  }

  std::vector<Int_t> getCoords(Int_t etaMin, Int_t energy){
    std::vector<Int_t> coords;
    std::cout << "Initializing coords vector, etaMin = " << etaMin << std::endl;
    for (unsigned int iEta =0; iEta < sizeof(etaPoints); iEta ++){
      if (etaPoints[iEta] == etaMin){
        coords.push_back(iEta);
        break;
      }
    }
    std::cout << "Initializing coords vector, energy = " << energy << std::endl;
    for (unsigned int iEnergy =0; iEnergy < sizeof(energyPoints); iEnergy ++){
      if (energyPoints[iEnergy] == energy){
        coords.push_back(iEnergy);
        break;
      }
    }

    return coords;
  }

  //your variables and branches here + init them in Init
  //for pointers, intialize to 0

  std::vector<Float_t>  *muonEntry_Energy=0;
  TBranch        *b_muonEntry_Energy=0; //!

  std::vector<Float_t>  *muonEntry_Momentum_X=0;
  TBranch        *b_muonEntry_Momentum_X=0; //

  std::vector<Float_t>  *muonEntry_Momentum_Y=0;
  TBranch        *b_muonEntry_Momentum_Y=0; //

  std::vector<Float_t>  *muonEntry_Momentum_Z=0;
  TBranch        *b_muonEntry_Momentum_Z=0; //

  std::vector<Float_t>  *muonEntry_Position_X=0;
  TBranch        *b_muonEntry_Position_X=0; //

  std::vector<Float_t>  *muonEntry_Position_Y=0;
  TBranch        *b_muonEntry_Position_Y=0; //

  std::vector<Float_t>  *muonEntry_Position_Z=0;
  TBranch        *b_muonEntry_Position_Z=0; //

  std::vector<Int_t>  *muonEntry_PDG_Code=0;
  TBranch        *b_muonEntry_PDG_Code=0; //

  std::vector<Float_t>  *TruthE=0;
  TBranch        *b_TruthE=0; //

  std::vector<Float_t>  *TruthPx=0;
  TBranch        *b_TruthPx=0; //

  std::vector<Float_t>  *TruthPy=0;
  TBranch        *b_TruthPy=0; //

  std::vector<Float_t>  *TruthPz=0;
  TBranch        *b_TruthPz=0; //

  std::vector<Float_t >   *newTTC_IDCaloBoundary_eta=0;
  TBranch        *b_newTTC_IDCaloBoundary_eta=0; //

  std::vector<Float_t >   *newTTC_IDCaloBoundary_phi=0;
  TBranch        *b_newTTC_IDCaloBoundary_phi=0; //

  std::vector<Float_t >   *newTTC_IDCaloBoundary_r=0;
  TBranch        *b_newTTC_IDCaloBoundary_r=0; //

  std::vector<Float_t >   *newTTC_IDCaloBoundary_z=0;
  TBranch        *b_newTTC_IDCaloBoundary_z=0; //
  //some output aux variables

  std::vector<Int_t> * TruthPDG = 0;
  TBranch *b_TruthPDG = 0;
  Int_t o_pdgid;

  Float_t o_energy;

  Int_t o_eventNumber;

  Int_t o_etaMin;
  Int_t o_etaMax;

  //Correlations NN
  Int_t input_momentum;
  TBranch *b_input_momentum;

  Float_t total_cell_energy;
  TBranch *b_total_cell_energy;

  std::vector<Float_t>  * cell_energy=0;
  TBranch        *b_cell_energy=0; //!


  //Kinematics branches
  Int_t kinBranch_pdgID;
  Float_t kinBranch_energy;
  Float_t kinBranch_deflectionAngle_theta;
  Float_t kinBranch_deflectionAngle_phi;
  Float_t kinBranch_momDeflectionAngle_theta;
  Float_t kinBranch_momDeflectionAngle_phi;

};

#endif
