#include "FCSNTUPAnaSkeleton.h"

using std::cout;
using std::endl;
using std::string;

void print_help(){
  cout << __FILE__ << " "<<endl;
  cout << __FILE__ << " Please provide : "<<endl;
  cout << __FILE__ << " -i : name of input file to analyse "<< endl;
  cout << __FILE__ << " -o : name of file in which output histos should go "<< endl;
  cout << __FILE__ << " -t : name of tree to analyse ; "<< endl;
  cout << __FILE__ << " -pdgID : input pdg of sample ; "<< endl;
  cout << __FILE__ << " -energy : input energy of sample (MeV); "<< endl;
  cout << __FILE__ << " -etaMin : input etaRange of sample (e.g. 100) ; "<< endl;
  cout << __FILE__ << " -etaMax : input etaRange of sample (e.g. 105) ; "<< endl;
  cout << __FILE__ << " -flagFileName : name of file containing tree flags containing eventNumber and any flag you want to use (e.g. hasxAODMuon); "<< endl;
  cout << __FILE__ << " -flag : name of the flag you want to use (e.g. hasxAODMuon); "<< endl;
  cout << __FILE__ << " -flagBool : bool whether you want to save events where flag is 1 or 0; "<< endl;
}

int main(int _argc, char **_argv) {

  string tInFileName="";
  string tTreeName="";
  string tOutFileName="";
  string tPDGID="";
  string tEnergy="";
  string tEtaMin="";
  string tEtaMax="";
  string tFlagFileName="";
  string tFlag="";
  string tFlagBool="";
  string tMaxEvents="";

  int optind(1);
  while ((optind < _argc)) {
    if(_argv[optind][0]!='-'){++optind; continue;}
    std::string sw = _argv[optind];
    if (sw == "-i") { ++optind; tInFileName=_argv[optind];}
    else if (sw == "-t") { ++optind; tTreeName=_argv[optind];}
    else if (sw == "-o") { ++optind; tOutFileName=_argv[optind];}
    else if (sw == "-pdgID") { ++optind; tPDGID=_argv[optind];}
    else if (sw == "-energy") { ++optind; tEnergy=_argv[optind];}
    else if (sw == "-etaMin") { ++optind; tEtaMin=_argv[optind];}
    else if (sw == "-etaMax") { ++optind; tEtaMax=_argv[optind];}
    else if (sw == "-flagFileName") { ++optind; tFlagFileName=_argv[optind];}
    else if (sw == "-flag") { ++optind; tFlag=_argv[optind];}
    else if (sw == "-flagBool") { ++optind; tFlagBool=_argv[optind];}
    else if (sw == "-maxEvents") { ++optind; tMaxEvents=_argv[optind];}
    else if (sw == "-h") { ++optind; print_help(); return 0;}
    else {
      cout << __FILE__ << " arg not known : "<< _argv[optind] << endl;
      print_help();
      return 1;
    }
    ++optind;
  }

  if ( "" == tInFileName || "" == tTreeName || "" == tPDGID || "" == tEnergy || "" == tEtaMin || "" == tEtaMax) {
    cout << "missing arg" << endl;
    print_help();
  }

  if(tFlagBool==""){tFlagBool="0";} //since tFlagBool is defined as string here, necessary to redefine if it is not passed
  else{tFlagBool="1";} //guaranteed as tFlagBool in RunFCSNTUPAna.cxx is defined as empty string ""

  if(""!=tFlagFileName){
    if(""==tFlag || ""==tFlagBool){
        cout << "missing flag and flagBool args" << endl;
        print_help();
    }
  }

  int maxEvents = -1;
  if(""!=tMaxEvents){
      maxEvents = std::stoi(tMaxEvents);
  }

  FCSNTUPAnaSkeleton tNtupAnaSkeleton(tInFileName,tTreeName,tOutFileName,tPDGID,tEnergy,tEtaMin,tEtaMax,tFlagFileName,tFlag,tFlagBool);
  tNtupAnaSkeleton.fAnalyse(maxEvents);

  return 0;

}
