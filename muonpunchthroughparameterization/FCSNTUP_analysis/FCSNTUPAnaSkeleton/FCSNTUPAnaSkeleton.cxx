#include "FCSNTUPAnaSkeleton.h"
#include <TROOT.h>
#include <TLegend.h>
#include <unistd.h>
#include <stdio.h>
#include <sstream>

#define isDebug 1
#define Debug(x) if (isDebug) x;

using std::cout;
using std::endl;
using std::string;

FCSNTUPAnaSkeleton::FCSNTUPAnaSkeleton(const std::string& pInFileName, const std::string& pTreeName, const std::string& pOutFileName, const std::string& pPDGID,  const std::string& pEnergy,  const std::string& pEtaMin, const std::string& pEtaMax, const std::string& pFlagFileName, const std::string& pFlag, const std::string& pFlagBool, const std::string& pInFileList) {

  //Command lines for using vector of vectors
  //gInterpreter->EnableAutoLoading();
  //gROOT->ProcessLine("#pragma link C++ class vector<vector<int>>");

  Debug(cout << __FILE__ << "in FCSNTUPAnaSkeleton::FCSNTUPAnaSkeleton " << endl);

  //if one file
  if (""==pInFileList) {
    // get a tree to analyse
    TFile *tFile = new TFile(pInFileName.c_str(),"READ");
    if (!tFile || !tFile->IsOpen()) {
      cout << __FILE__<< " ERROR: file could not be opened : " << pInFileName << endl;
    }
    Debug(cout << __FILE__<< " fetching tree from file : " << pTreeName << " from " << pInFileName << endl);
    tFile->GetObject(pTreeName.c_str(),mTree);
  }
  //if we want to analyse a list of files
  else {
    if (FILE* tFile = fopen(pInFileList.c_str(), "r")) {
      fclose(tFile);
    }
    else {
      cout << __FILE__ << " ERROR: cannot read list of input files : " << endl;
      cout << pInFileList << endl;
    }
    Debug(cout << __FILE__ << " chaining file from list , tree : " << pInFileList << " , "<< pTreeName<< endl);

    std::ifstream tIFS(pInFileList.c_str());
    string tThisFile;
    TChain* tChain=new TChain(pTreeName.c_str());

    while(std::getline(tIFS, tThisFile)) {
      Debug(cout << " ..... "<< tThisFile <<endl);
      tChain->Add(tThisFile.c_str());
    }
    mTree=(TTree*)(tChain);
  }

  // initialize its branches
  Debug(cout << __FILE__ << " init tree " << endl);
  fInit(mTree);

  // setting output file name from command line
  mOutFileName=pOutFileName;
  mOutDir=mOutFileName.substr(0, mOutFileName.find_last_of("\\/"));

  // output slice info
  std::string prettySlice = pEnergy + " MeV " + pEtaMin +" < #eta < " + pEtaMax;
  cout << "Analyising Slice: "<< prettySlice << endl;

  // get variables and convert to int
  o_etaMin = std::stoi(pEtaMin);
  o_etaMax = std::stoi(pEtaMax);
  o_energy = std::stoi(pEnergy);

  // assign variables to be used later
  mFlagFileName=pFlagFileName;
  mFlag=pFlag;
  mFlagBool=std::stoi(pFlagBool);
  Debug(cout << "Flag boolean is set to: " << mFlagBool << endl);

  //Parse flag file variables, if flagfilename is passed
  if(""==pFlagFileName){return;}

  mFlagFile = new TFile(pFlagFileName.c_str(),"READ");
  mFlagTree = (TTree*)mFlagFile->Get("flags");
  mFlagTree->SetBranchAddress(mFlag.c_str(), &flag, &b_flag);
  mFlagTree->SetBranchAddress("truthEta", &truthEta, &b_truthEta);
  mFlagTree->SetBranchAddress("truthPhi", &truthPhi, &b_truthPhi);
  std::cout << "number of entries in flag files: "<< mFlagTree->GetEntries() << std::endl;
  std::cout << "Loading truth matching map..." << std::endl;
  for(unsigned int iFlagEntry = 0; iFlagEntry <  mFlagTree->GetEntries(); iFlagEntry++){
      mFlagTree->GetEntry(iFlagEntry);

      if(m_phi_flag_map.find(truthPhi) != m_phi_flag_map.end()){
          //std::cout << "entry is already in map: " << truthPhi << std::endl;
          if(m_phi_flag_map.find(truthPhi+1) != m_phi_flag_map.end()){
              //std::cout << "entry +1 is already in map: " << truthPhi << std::endl;
              if(m_phi_flag_map.find(truthPhi-1) != m_phi_flag_map.end()){
                  //std::cout << "entry -1 is already in map: " << truthPhi << std::endl;
              }else{
                  truthPhi = truthPhi - 1;
              }
          }else{

              truthPhi = truthPhi + 1;

          }
      }

      m_phi_flag_map.insert(std::pair<long long, Int_t>(truthPhi,flag));
      //std::cout << "adding to map: " << truthPhi << ", " << flag << std::endl;
  }

  std::cout << "number of map entries: " << m_phi_flag_map.size() << std::endl;


}

void FCSNTUPAnaSkeleton::fInit(TTree* pTree) {

  // init branches here
  pTree->SetBranchAddress("MuonEntryLayer_E", &muonEntry_Energy, &b_muonEntry_Energy);
  pTree->SetBranchAddress("MuonEntryLayer_px", &muonEntry_Momentum_X, &b_muonEntry_Momentum_X);
  pTree->SetBranchAddress("MuonEntryLayer_py", &muonEntry_Momentum_Y, &b_muonEntry_Momentum_Y);
  pTree->SetBranchAddress("MuonEntryLayer_pz", &muonEntry_Momentum_Z, &b_muonEntry_Momentum_Z);
  pTree->SetBranchAddress("MuonEntryLayer_x", &muonEntry_Position_X, &b_muonEntry_Position_X);
  pTree->SetBranchAddress("MuonEntryLayer_y", &muonEntry_Position_Y, &b_muonEntry_Position_Y);
  pTree->SetBranchAddress("MuonEntryLayer_z", &muonEntry_Position_Z, &b_muonEntry_Position_Z);
  pTree->SetBranchAddress("MuonEntryLayer_pdg", &muonEntry_PDG_Code, &b_muonEntry_PDG_Code);

  //Truth Branches
  pTree->SetBranchAddress("TruthE", &TruthE, &b_TruthE);
  pTree->SetBranchAddress("TruthPx", &TruthPx, &b_TruthPx);
  pTree->SetBranchAddress("TruthPy", &TruthPy, &b_TruthPy);
  pTree->SetBranchAddress("TruthPz", &TruthPz, &b_TruthPz);

  //Calo Entry Branches
  pTree->SetBranchAddress("newTTC_IDCaloBoundary_eta", &newTTC_IDCaloBoundary_eta, &b_newTTC_IDCaloBoundary_eta);
  pTree->SetBranchAddress("newTTC_IDCaloBoundary_phi", &newTTC_IDCaloBoundary_phi, &b_newTTC_IDCaloBoundary_phi);
  pTree->SetBranchAddress("newTTC_IDCaloBoundary_r", &newTTC_IDCaloBoundary_r, &b_newTTC_IDCaloBoundary_r);
  pTree->SetBranchAddress("newTTC_IDCaloBoundary_z", &newTTC_IDCaloBoundary_z, &b_newTTC_IDCaloBoundary_z);

  //Aux Branches
  pTree->SetBranchAddress("TruthPDG", &TruthPDG, &b_TruthPDG);
  pTree->SetBranchAddress("TruthE", &TruthE, &b_TruthE);


  //correlation branches
  pTree->SetBranchAddress("cell_energy", &cell_energy, &b_cell_energy);
  pTree->SetBranchAddress("total_cell_energy", &total_cell_energy, &b_total_cell_energy);

  return;
}

// loop over events and fill histograms
void FCSNTUPAnaSkeleton::fAnalyse(int pNevents) {

  // histograms of input branches
  TH1F h_energy = TH1F("energy","energy", 100, 0,10000);
  TH1F h_momentum_X = TH1F("momentum_X","momentum_X", 100, -5000,5000);
  TH1F h_momentum_Y = TH1F("momentum_Y","momentum_Y", 100, -5000,5000);
  TH1F h_momentum_Z = TH1F("momentum_Z","momentum_Z", 100, -5000,5000);
  TH1F h_position_X = TH1F("position_X","position_X", 100, -5000,5000);
  TH1F h_position_Y = TH1F("position_Y","position_Y", 100, -5000,5000);
  TH1F h_position_Z = TH1F("position_Z","position_Z", 100, -10000,10000);
  TH1F h_PDG_Code = TH1F("PDG_Code","PDG_Code", 6000, -3000,3000);

  //Histograms for param
  //Particle Frequencies
  TH1F h_photon_Freq = TH1F("NumExitPDG22","NumExitPDG22", 20, 0, 20);
  TH1F h_proton_Freq = TH1F("NumExitPDG2212","NumExitPDG2212", 20, 0, 20);
  TH1F h_pion_Freq = TH1F("NumExitPDG211","NumExitPDG211", 20, 0, 20);
  TH1F h_elec_Freq = TH1F("NumExitPDG11","NumExitPDG11", 20, 0, 20);
  TH1F h_muon_Freq = TH1F("NumExitPDG13","NumExitPDG13", 20, 0, 20);

  //Particle Correlations
  TH2F h_2d_proton_pion = TH2F("h_2d_proton_pion","h_2d_proton_pion",50,0,50,50,0,50);
  TH2F h_2d_elec_photon = TH2F("h_2d_elec_photon","h_2d_elec_photon",40,0,40,50,0,50);

  //Particle Energies
  TH1F h_photon_Energy = TH1F("ExitEnergyPDG22","ExitEnergyPDG22", 375, 0, 75000);
  TH1F h_proton_Energy = TH1F("ExitEnergyPDG2212","ExitEnergyPDG2212", 375, 0, 75000);
  TH1F h_pion_Energy = TH1F("ExitEnergyPDG211","ExitEnergyPDG211", 375, 0, 75000);
  TH1F h_elec_Energy = TH1F("ExitEnergyPDG11","ExitEnergyPDG11", 375, 0, 75000);
  TH1F h_muon_Energy = TH1F("ExitEnergyPDG13","ExitEnergyPDG13", 375, 0, 75000);

  //1D Deflection Angles
  TH1F h_delta_theta_photon = TH1F("h_delta_theta_photon","h_delta_theta_photon", 50, 0, TMath::Pi()/2.);
  TH1F h_delta_theta_proton = TH1F("h_delta_theta_proton","h_delta_theta_proton", 50, 0, TMath::Pi()/2.);
  TH1F h_delta_theta_pion = TH1F("h_delta_theta_pion","h_delta_theta_pion", 50, 0, TMath::Pi()/2.);
  TH1F h_delta_theta_elec = TH1F("h_delta_theta_elec","h_delta_theta_elec", 50, 0, TMath::Pi()/2.);
  TH1F h_delta_theta_muon = TH1F("h_delta_theta_muon","h_delta_theta_muon", 50, 0, TMath::Pi()/2.);

  TH1F h_delta_phi_photon = TH1F("h_delta_phi_photon","h_delta_phi_photon", 50, 0, TMath::Pi()/2.);
  TH1F h_delta_phi_proton = TH1F("h_delta_phi_proton","h_delta_phi_proton", 50, 0, TMath::Pi()/2.);
  TH1F h_delta_phi_pion = TH1F("h_delta_phi_pion","h_delta_phi_pion", 50, 0, TMath::Pi()/2.);
  TH1F h_delta_phi_elec = TH1F("h_delta_phi_elec","h_delta_phi_elec", 50, 0, TMath::Pi()/2.);
  TH1F h_delta_phi_muon = TH1F("h_delta_phi_muon","h_delta_phi_muon", 50, 0, TMath::Pi()/2.);

  TH1F h_mom_delta_theta_photon = TH1F("h_mom_delta_theta_photon","h_mom_delta_theta_photon", 50, 0, TMath::Pi()/2.);
  TH1F h_mom_delta_theta_proton = TH1F("h_mom_delta_theta_proton","h_mom_delta_theta_proton", 50, 0, TMath::Pi()/2.);
  TH1F h_mom_delta_theta_pion = TH1F("h_mom_delta_theta_pion","h_mom_delta_theta_pion", 50, 0, TMath::Pi()/2.);
  TH1F h_mom_delta_theta_elec = TH1F("h_mom_delta_theta_elec","h_mom_delta_theta_elec", 50, 0, TMath::Pi()/2.);
  TH1F h_mom_delta_theta_muon = TH1F("h_mom_delta_theta_muon","h_mom_delta_theta_muon", 50, 0, TMath::Pi()/2.);

  TH1F h_mom_delta_phi_proton = TH1F("h_mom_delta_phi_proton","h_mom_delta_phi_proton", 50, 0, TMath::Pi()/2.);
  TH1F h_mom_delta_phi_photon = TH1F("h_mom_delta_phi_photon","h_mom_delta_phi_photon", 50, 0, TMath::Pi()/2.);
  TH1F h_mom_delta_phi_pion = TH1F("h_mom_delta_phi_pion","h_mom_delta_phi_pion", 50, 0, TMath::Pi()/2.);
  TH1F h_mom_delta_phi_elec = TH1F("h_mom_delta_phi_elec","h_mom_delta_phi_elec", 50, 0, TMath::Pi()/2.);
  TH1F h_mom_delta_phi_muon = TH1F("h_mom_delta_phi_muon","h_mom_delta_phi_muon", 50, 0, TMath::Pi()/2.);

  //2D Deflection Angles
  TH2F h_enery_v_delta_phi_proton = TH2F("ExitDeltaPhiPDG22","ExitDeltaPhiPDG22", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_delta_phi_photon = TH2F("ExitDeltaPhiPDG2212","ExitDeltaPhiPDG2212", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_delta_phi_pion = TH2F("ExitDeltaPhiPDG211","ExitDeltaPhiPDG211", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_delta_phi_elec = TH2F("ExitDeltaPhiPDG11","ExitDeltaPhiPDG11", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_delta_phi_muon = TH2F("ExitDeltaPhiPDG13","ExitDeltaPhiPDG13", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);

  TH2F h_enery_v_mom_delta_phi_proton = TH2F("MomDeltaPhiPDG22","MomDeltaPhiPDG22", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_mom_delta_phi_photon = TH2F("MomDeltaPhiPDG2212","MomDeltaPhiPDG2212", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_mom_delta_phi_pion = TH2F("MomDeltaPhiPDG211","MomDeltaPhiPDG211", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_mom_delta_phi_elec = TH2F("MomDeltaPhiPDG11","MomDeltaPhiPDG11", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_mom_delta_phi_muon = TH2F("MomDeltaPhiPDG13","MomDeltaPhiPDG13", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);

  TH2F h_enery_v_delta_theta_proton = TH2F("ExitDeltaThetaPDG22","ExitDeltaThetaPDG22", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_delta_theta_photon = TH2F("ExitDeltaThetaPDG2212","ExitDeltaThetaPDG2212", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_delta_theta_pion = TH2F("ExitDeltaThetaPDG211","ExitDeltaThetaPDG211", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_delta_theta_elec = TH2F("ExitDeltaThetaPDG11","ExitDeltaThetaPDG11", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_delta_theta_muon = TH2F("ExitDeltaThetaPDG13","ExitDeltaThetaPDG13", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);

  TH2F h_enery_v_mom_delta_theta_proton = TH2F("MomDeltaThetaPDG22","MomDeltaThetaPDG22", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_mom_delta_theta_photon = TH2F("MomDeltaThetaPDG2212","MomDeltaThetaPDG2212", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_mom_delta_theta_pion = TH2F("MomDeltaThetaPDG211","MomDeltaThetaPDG211", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_mom_delta_theta_elec = TH2F("MomDeltaThetaPDG11","MomDeltaThetaPDG11", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);
  TH2F h_enery_v_mom_delta_theta_muon = TH2F("MomDeltaThetaPDG13","MomDeltaThetaPDG13", 375, 0, 75000, 50, 0,  TMath::Pi()/2.);

  //set eta values, stored into etaPoints[] array, see FCSNTUPAnaSkeleton.h for implementation
  set_eta_points();
  //calculate no of points by sizeof() method
  int numEtaPoints = sizeof(etaPoints)/sizeof(etaPoints[0]);
  int numEnergyPoints = sizeof(energyPoints)/sizeof(energyPoints[0]);

  //Create histogram entry of punch through inputPt vs eta vs punch through prob
  TH2F h_inputPt_v_eta_v_punchThroProb = TH2F("h_inputPt_v_eta_v_punchThroProb","h_inputPt_v_eta_v_punchThroProb", numEtaPoints, 0, numEtaPoints, numEnergyPoints, 0,  numEnergyPoints);
  //set eta and energy bins, into axis->SetBinLabel by using etaPoints[bins] and energyPoints[bins] array, see FCSNTUPAnaSkeleton.h for implementation
  set_eta_bin_values(h_inputPt_v_eta_v_punchThroProb.GetXaxis());
  set_energy_bin_values(h_inputPt_v_eta_v_punchThroProb.GetYaxis());
  //get 2-element vector of etaMin, energy given input etaMin and energy, and output this coord vector, see FCSNTUPAnaSkeleton.h for implementation
  std::vector<Int_t> coords = getCoords(o_etaMin, o_energy);

  // create further TH2F plots
  TH2F h_r_z_muonEntryLayer = TH2F("h_r_z_muonEntryLayer","h_r_z_muonEntryLayer", 200, -10000,  10000, 100, 0, 10000);
  TH2F h_caloNormal = TH2F("h_caloNormal","h_caloNormal", 50, -1,  1, 50, -1, 1);

  //create two cylinders to describe muonEntryLayer
  new TGeoManager("muonEntryCylinders","muonEntryCylinders");
  TGeoTube * cylinderBarrel = new TGeoTube(0., 4255., 6550.); //Barrel Region
  TGeoTube * cylinderEndcap = new TGeoTube(0., 3800., 6735.);

  // get entries based on mTree
  int tAllEntries = (pNevents<0) ? mTree->GetEntries() : pNevents;

  // declare new out ttree and tfile
  TFile* tOutFile = new TFile(mOutFileName.c_str(), "recreate");
  TTree * auxTree = new TTree("AuxTree", "AuxTree");

  //create output branches and trees
  auxTree->Branch("AuxPDGID", &o_pdgid);
  auxTree->Branch("AuxEnergy", &o_energy);
  auxTree->Branch("AuxEtaMin", &o_etaMin);
  auxTree->Branch("AuxEtaMax", &o_etaMax);

  TTree * corrVarsTree = new TTree("corrVarsTree", "corrVarsTree");
  corrVarsTree->Branch("AuxEtaMin", &o_etaMin);
  corrVarsTree->Branch("input_momentum", &input_momentum);
  input_momentum = o_energy;
  corrVarsTree->Branch("newTTC_IDCaloBoundary_eta", &newTTC_IDCaloBoundary_eta);
  corrVarsTree->Branch("newTTC_IDCaloBoundary_phi", &newTTC_IDCaloBoundary_phi);
  corrVarsTree->Branch("total_cell_energy", &total_cell_energy);
  std::vector<Float_t> o_cell_energy(vCellBranchNames.size(), 0.0);

  // see definition in setUpBranches method in FCSNTUPAnaSkeleton.h and below for implementation
  setUpBranches(corrVarsTree, o_cell_energy, vCellBranchNames);

  TTree * kinematicsTree = new TTree("kinematicsTree", "kinematicsTree");
  kinematicsTree->Branch("eventNumber", &o_eventNumber);
  kinematicsTree->Branch("AuxEtaMin", &o_etaMin);
  kinematicsTree->Branch("aux_input_momentum", &input_momentum);
  kinematicsTree->Branch("kinBranch_pdgID", &kinBranch_pdgID);
  kinematicsTree->Branch("kinBranch_energy", &kinBranch_energy);
  kinematicsTree->Branch("kinBranch_deflectionAngle_theta", &kinBranch_deflectionAngle_theta);
  kinematicsTree->Branch("kinBranch_deflectionAngle_phi", &kinBranch_deflectionAngle_phi);
  kinematicsTree->Branch("kinBranch_momDeflectionAngle_theta", &kinBranch_momDeflectionAngle_theta);
  kinematicsTree->Branch("kinBranch_momDeflectionAngle_phi", &kinBranch_momDeflectionAngle_phi);

  if(mFlagBool){
    if(mFlagTree){
        //check entries in mtree similar or not to mFlagTree
        if(tAllEntries != mFlagTree->GetEntries()){
            std::cout << "NTup file and flag file have different number of entries, exiting" << std::endl;
            return;
        }
    }    
  }

  //initialize, to calculate number of segment has punch through that passes cut
  int numHasPunchThrough = 0;

  //start analyzing entries
  Debug(cout << __FILE__<< "Analyising # entries : "<<tAllEntries<<endl);
  //loop over entries in mtree
  for (int iEntry = 0; iEntry < tAllEntries; ++iEntry) {
    //associate branch variable to the entry from mTree
    o_eventNumber = iEntry;    

    //get entry from mtree
    mTree->GetEntry(iEntry);
    if(mFlagBool){
      if(mFlagTree){
          //get entry from mFlagTree
          mFlagTree->GetEntry(iEntry);
      }      
    }

    if (isDebug){
      if (0==iEntry%100){
        cout << " .... processing entry "<< iEntry << endl;
      }
    }

    //flag matching procedure, only if the flagbool is on, otherwise consider as if no matching is done
    if(mFlagBool){
      if(mFlagTree){
          long long ntup_truthPhi = llround(float(newTTC_IDCaloBoundary_phi->at(0)*1000000));
          int foundFlag = -1;
          //std::cout << "trying to find: "<< ntup_truthPhi << std::endl;
          if ( m_phi_flag_map.find(ntup_truthPhi) != m_phi_flag_map.end() ) {
              foundFlag = m_phi_flag_map[ntup_truthPhi];
              //std::cout << "found! flag is: " << m_phi_flag_map[ntup_truthPhi] << std::endl;
          } else {
              //std::cout << "couldn't find in map, trying to find +1" << std::endl;
              if ( m_phi_flag_map.find(ntup_truthPhi + 1) != m_phi_flag_map.end() ){
                  foundFlag = m_phi_flag_map[ntup_truthPhi+1];
                  //std::cout << "found! flag is: " << foundFlag << std::endl;
              }else{
                  //std::cout << "couldn't find in map, trying to find -1" << std::endl;

                  if ( m_phi_flag_map.find(ntup_truthPhi - 1) != m_phi_flag_map.end() ){
                      foundFlag = m_phi_flag_map[ntup_truthPhi-1];
                      //std::cout << "found! flag is: " << foundFlag<< std::endl;
                  }else{
                      //std::cout << "couldn't find any flags, skipping this event" << std::endl;
                  }
              }
            }
            if(foundFlag != mFlagBool){continue;} //if we found a flag that doesn't match our request skip
      }      
    }

    //start initializing, to calculate number of secondary (punch through) particles
    int numPions = 0;
    int numProtons = 0;
    int numElecs = 0;
    int numPhotons = 0;
    int numMuons = 0;

    //Used for investigating whether an event created a punch through
    //use muonEntry_Energy for looping this, possibly can use muonEntry_PDG_Code?
    for (unsigned int iMuonEntry = 0; iMuonEntry < muonEntry_Energy->size(); iMuonEntry ++) {
      // see FCSNTUPAnaSkeleton.h for minPunchThruEnergyCut
      if (muonEntry_Energy->at(iMuonEntry) > minPunchThruEnergyCut){
        numHasPunchThrough ++;
        break;
      }
    }

    //Create Vectors (momentum and position) for incoming particle
    //momentum
    TLorentzVector VCaloEntryMomentum;
    VCaloEntryMomentum.SetPxPyPzE(TruthPx->at(0),TruthPy->at(0),TruthPz->at(0),TruthE->at(0));
    //position
    TVector3 VCaloEntryPosition;
    VCaloEntryPosition.SetMagThetaPhi(1., 2*atan(exp(-newTTC_IDCaloBoundary_eta->at(0))), newTTC_IDCaloBoundary_phi->at(0));


    //assign cell energy vector 
    //o_cell_energy = *cell_energy;

    for (unsigned int i = 0; i < 24; i++){ //loop through first 24 elements in cell_energy only these are used
        o_cell_energy.at(i) = cell_energy->at(i);
    }

    //see FCSNTUPAnaSkeleton.h for definition of DivideVectorByScalar and below for implementation
    DivideVectorByScalar(o_cell_energy, total_cell_energy);

    //use muonEntry_Energy for looping this, possibly can use muonEntry_PDG_Code?
    for (unsigned int iMuonEntry = 0; iMuonEntry < muonEntry_Energy->size(); iMuonEntry ++) {
      //create TVector3 of momemtum dir and position for the punch through particles, from histogram variables in fInit
      TVector3 VPunchThroughMomDir(muonEntry_Momentum_X->at(iMuonEntry),muonEntry_Momentum_Y->at(iMuonEntry),muonEntry_Momentum_Z->at(iMuonEntry));
      TVector3 VPunchThroughPosition(muonEntry_Position_X->at(iMuonEntry),muonEntry_Position_Y->at(iMuonEntry),muonEntry_Position_Z->at(iMuonEntry));
      //punch through R and Z
      float punchTrough_R = sqrt(VPunchThroughPosition.x()*VPunchThroughPosition.x() + VPunchThroughPosition.y()*VPunchThroughPosition.y());
      float punchTrough_Z = VPunchThroughPosition.Z();

      //Apply Cuts
      //apply cut on R-Z to remove inner hits.
      if(punchTrough_R < 3000 && abs(punchTrough_Z) < 6730 ){
        continue;
      }
      

      //apply cut on particles that are travelling towards detector centre
      Double_t point[3] = {VPunchThroughPosition.x(), VPunchThroughPosition.y(), VPunchThroughPosition.Z()};
      Double_t norm[3] = {0., 0., 0.};
      Double_t angle;
      if(punchTrough_R > cylinderEndcap->GetRmax() ){
          cylinderBarrel->ComputeNormal(point, point, norm);
          h_caloNormal.Fill(norm[0], norm[1]);
          TVector3 vNorm(norm[0], norm[1], norm[2]);
          angle = vNorm.Angle(VPunchThroughMomDir);
      }
      else{
          cylinderBarrel->ComputeNormal(point, point, norm);
          TVector3 vNorm(norm[0], norm[1], norm[2]);
          angle = vNorm.Angle(VPunchThroughMomDir);
      }
      if(angle > TMath::Pi()/2){continue;} //skip particle if tavelling back into detector

      h_r_z_muonEntryLayer.Fill(VPunchThroughPosition.Z(), sqrt(VPunchThroughPosition.x()*VPunchThroughPosition.x() + VPunchThroughPosition.y()*VPunchThroughPosition.y()));

      //Fill basic variables histograms
      h_energy.Fill(muonEntry_Energy->at(iMuonEntry));
      h_momentum_X.Fill(muonEntry_Momentum_X->at(iMuonEntry));
      h_momentum_Y.Fill(muonEntry_Momentum_Y->at(iMuonEntry));
      h_momentum_Z.Fill(muonEntry_Momentum_Z->at(iMuonEntry));
      h_position_X.Fill(muonEntry_Position_X->at(iMuonEntry));
      h_position_Y.Fill(muonEntry_Position_Y->at(iMuonEntry));
      h_position_Z.Fill(muonEntry_Position_Z->at(iMuonEntry));
      h_PDG_Code.Fill(muonEntry_PDG_Code->at(iMuonEntry));


      //set Kinematics branches
      kinBranch_pdgID = muonEntry_PDG_Code->at(iMuonEntry);
      kinBranch_energy = muonEntry_Energy->at(iMuonEntry);
      kinBranch_deflectionAngle_theta = fmod(fabs(VPunchThroughPosition.Theta() - VCaloEntryPosition.Theta()), TMath::Pi()/2);
      kinBranch_deflectionAngle_phi = fmod(fabs(VPunchThroughPosition.Phi() - VCaloEntryPosition.Phi()), TMath::Pi()/2);
      kinBranch_momDeflectionAngle_theta = fmod(fabs(VPunchThroughMomDir.Theta() - VPunchThroughPosition.Theta()), TMath::Pi()/2);
      kinBranch_momDeflectionAngle_phi = fmod(fabs(VPunchThroughMomDir.Phi() - VPunchThroughPosition.Phi()), TMath::Pi()/2);

      kinematicsTree->Fill(); // fill for every punch through particle

      //Check for each particle type and fill relevant histograms
      if(abs(muonEntry_PDG_Code->at(iMuonEntry)) == 22){ //Photon
        h_photon_Energy.Fill(muonEntry_Energy->at(iMuonEntry));
        h_delta_theta_photon.Fill(fabs(VPunchThroughPosition.Theta() - VCaloEntryPosition.Theta()));
        h_delta_phi_photon.Fill(fabs(VPunchThroughPosition.Phi() - VCaloEntryPosition.Phi()));
        h_mom_delta_theta_photon.Fill(fabs(VPunchThroughMomDir.Theta() - VPunchThroughPosition.Theta()));
        h_mom_delta_phi_photon.Fill(fabs(VPunchThroughMomDir.Phi() - VPunchThroughPosition.Phi()));
        h_enery_v_delta_theta_photon.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughPosition.Theta() - VCaloEntryPosition.Theta()));
        h_enery_v_mom_delta_theta_photon.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughMomDir.Theta() - VPunchThroughPosition.Theta()));
        h_enery_v_delta_phi_photon.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughPosition.Phi() - VCaloEntryPosition.Phi()));
        h_enery_v_mom_delta_phi_photon.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughMomDir.Phi() - VPunchThroughPosition.Phi()));
        numPhotons++;
      }
      if(abs(muonEntry_PDG_Code->at(iMuonEntry)) == 2212){ //Proton
      h_proton_Energy.Fill(muonEntry_Energy->at(iMuonEntry));
        h_delta_theta_proton.Fill(fabs(VPunchThroughPosition.Theta() - VCaloEntryPosition.Theta()));
        h_delta_phi_proton.Fill(fabs(VPunchThroughPosition.Phi() - VCaloEntryPosition.Phi()));
        h_mom_delta_theta_proton.Fill(fabs(VPunchThroughMomDir.Theta() - VPunchThroughPosition.Theta()));
        h_mom_delta_phi_proton.Fill(fabs(VPunchThroughMomDir.Phi() - VPunchThroughPosition.Phi()));
        h_enery_v_delta_theta_proton.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughPosition.Theta() - VCaloEntryPosition.Theta()));
        h_enery_v_mom_delta_theta_proton.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughMomDir.Theta() - VPunchThroughPosition.Theta()));
        h_enery_v_delta_phi_proton.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughPosition.Phi() - VCaloEntryPosition.Phi()));
        h_enery_v_mom_delta_phi_proton.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughMomDir.Phi() - VPunchThroughPosition.Phi()));
        numProtons++;
      }
      if(abs(muonEntry_PDG_Code->at(iMuonEntry)) == 211){ //pion
        h_pion_Energy.Fill(muonEntry_Energy->at(iMuonEntry));
        h_delta_theta_pion.Fill(fabs(VPunchThroughPosition.Theta() - VCaloEntryPosition.Theta()));
        h_delta_phi_pion.Fill(fabs(VPunchThroughPosition.Phi() - VCaloEntryPosition.Phi()));
        h_mom_delta_theta_pion.Fill(fabs(VPunchThroughMomDir.Theta() - VPunchThroughPosition.Theta()));
        h_mom_delta_phi_pion.Fill(fabs(VPunchThroughMomDir.Phi() - VPunchThroughPosition.Phi()));
        h_enery_v_delta_theta_pion.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughPosition.Theta() - VCaloEntryPosition.Theta()));
        h_enery_v_mom_delta_theta_pion.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughMomDir.Theta() - VPunchThroughPosition.Theta()));
        h_enery_v_delta_phi_pion.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughPosition.Phi() - VCaloEntryPosition.Phi()));
        h_enery_v_mom_delta_phi_pion.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughMomDir.Phi() - VPunchThroughPosition.Phi()));
        numPions++;
      }
      if(abs(muonEntry_PDG_Code->at(iMuonEntry)) == 11){ //Electron
        h_elec_Energy.Fill(muonEntry_Energy->at(iMuonEntry));
        h_delta_theta_elec.Fill(fabs(VPunchThroughPosition.Theta() - VCaloEntryPosition.Theta()));
        h_delta_phi_elec.Fill(fabs(VPunchThroughPosition.Phi() - VCaloEntryPosition.Phi()));
        h_mom_delta_theta_elec.Fill(fabs(VPunchThroughMomDir.Theta() - VPunchThroughPosition.Theta()));
        h_mom_delta_phi_elec.Fill(fabs(VPunchThroughMomDir.Phi() - VPunchThroughPosition.Phi()));
        h_enery_v_delta_theta_elec.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughPosition.Theta() - VCaloEntryPosition.Theta()));
        h_enery_v_mom_delta_theta_elec.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughMomDir.Theta() - VPunchThroughPosition.Theta()));
        h_enery_v_delta_phi_elec.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughPosition.Phi() - VCaloEntryPosition.Phi()));
        h_enery_v_mom_delta_phi_elec.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughMomDir.Phi() - VPunchThroughPosition.Phi()));
        numElecs++;
      }
      if(abs(muonEntry_PDG_Code->at(iMuonEntry)) == 13){ //Muon
        h_muon_Energy.Fill(muonEntry_Energy->at(iMuonEntry));
        h_delta_theta_muon.Fill(fabs(VPunchThroughPosition.Theta() - VCaloEntryPosition.Theta()));
        h_delta_phi_muon.Fill(fabs(VPunchThroughPosition.Phi() - VCaloEntryPosition.Phi()));
        h_mom_delta_theta_muon.Fill(fabs(VPunchThroughMomDir.Theta() - VPunchThroughPosition.Theta()));
        h_mom_delta_phi_muon.Fill(fabs(VPunchThroughMomDir.Phi() - VPunchThroughPosition.Phi()));
        h_enery_v_delta_theta_muon.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughPosition.Theta() - VCaloEntryPosition.Theta()));
        h_enery_v_mom_delta_theta_muon.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughMomDir.Theta() - VPunchThroughPosition.Theta()));
        h_enery_v_delta_phi_muon.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughPosition.Phi() - VCaloEntryPosition.Phi()));
        h_enery_v_mom_delta_phi_muon.Fill(muonEntry_Energy->at(iMuonEntry), fabs(VPunchThroughMomDir.Phi() - VPunchThroughPosition.Phi()));
        numMuons++;
      }
    }

    //Particle Frequencies
    h_photon_Freq.Fill(numPhotons);
    h_proton_Freq.Fill(numProtons);
    h_pion_Freq.Fill(numPions);
    h_elec_Freq.Fill(numElecs);
    h_muon_Freq.Fill(numMuons);

    //Particle Correlations
    h_2d_proton_pion.Fill(numPions, numProtons);
    h_2d_elec_photon.Fill(numPhotons, numElecs);

    o_pdgid = TruthPDG->at(0);
    o_energy = TruthE->at(0);
    auxTree->Fill();
    corrVarsTree->Fill();
  }

  if(mFlagBool){
    if(mFlagFile){mFlagFile->Close();}    
  }

  //get ratio of all punch thorugh that passes the energy cut and all entries in mTree
  h_inputPt_v_eta_v_punchThroProb.Fill(coords.at(0), coords.at(1), (float)numHasPunchThrough/(float)tAllEntries);

  //define output plot names
  string tOutFilePlots = (tOutFile->GetName());
  tOutFilePlots.erase(tOutFilePlots.length()-5);
  tOutFilePlots = tOutFilePlots + "/";

  Debug(cout << __FILE__<< " fAnalyse all done, writing to file :  "<< tOutFile->GetName() << endl);

  //Write Histograms
  h_energy.Write();
  h_momentum_X.Write();
  h_momentum_Y.Write();
  h_momentum_Z.Write();
  h_position_X.Write();
  h_position_Y.Write();
  h_position_Z.Write();
  h_PDG_Code.Write();

  checkHistAndWrite1D(h_photon_Freq);
  checkHistAndWrite1D(h_proton_Freq);
  checkHistAndWrite1D(h_pion_Freq);
  checkHistAndWrite1D(h_elec_Freq);
  checkHistAndWrite1D(h_muon_Freq);

  //Particle Correlations
  checkHistAndWrite2D(h_2d_proton_pion);
  checkHistAndWrite2D(h_2d_elec_photon);

  //Particle Correlations no Zero
  //checkHistAndWrite1D(h_2d_proton_pion_no_zero);
  //checkHistAndWrite1D(h_2d_elec_photon_no_zero);

  //Particle Energies
  checkHistAndWrite1D(h_photon_Energy);
  checkHistAndWrite1D(h_proton_Energy);
  checkHistAndWrite1D(h_pion_Energy);
  checkHistAndWrite1D(h_elec_Energy);
  checkHistAndWrite1D(h_muon_Energy);

  //Deflection Angles
  //checkHistAndWrite1D(h_delta_theta_photon);
  //checkHistAndWrite1D(h_delta_theta_proton);
  //checkHistAndWrite1D(h_delta_theta_pion);
  //checkHistAndWrite1D(h_delta_theta_elec);
  //checkHistAndWrite1D(h_delta_theta_muon);

  //checkHistAndWrite1D(h_delta_phi_photon);
  //checkHistAndWrite1D(h_delta_phi_proton);
  //checkHistAndWrite1D(h_delta_phi_pion);
  //checkHistAndWrite1D(h_delta_phi_elec);
  //checkHistAndWrite1D(h_delta_phi_muon);

  //Momentum Direction
  //checkHistAndWrite1D(h_mom_delta_theta_photon);
  //checkHistAndWrite1D(h_mom_delta_theta_proton);
  //checkHistAndWrite1D(h_mom_delta_theta_pion);
  //checkHistAndWrite1D(h_mom_delta_theta_elec);
  //checkHistAndWrite1D(h_mom_delta_theta_muon);

  //checkHistAndWrite1D(h_mom_delta_phi_photon);
  //checkHistAndWrite1D(h_mom_delta_phi_proton);
  //checkHistAndWrite1D(h_mom_delta_phi_pion);
  //checkHistAndWrite1D(h_mom_delta_phi_elec);
  //checkHistAndWrite1D(h_mom_delta_phi_muon);

  checkHistAndWrite2D(h_enery_v_delta_theta_proton);
  checkHistAndWrite2D(h_enery_v_delta_theta_photon);
  checkHistAndWrite2D(h_enery_v_delta_theta_pion);
  checkHistAndWrite2D(h_enery_v_delta_theta_elec);
  checkHistAndWrite2D(h_enery_v_delta_theta_muon);

  checkHistAndWrite2D(h_enery_v_mom_delta_theta_proton);
  checkHistAndWrite2D(h_enery_v_mom_delta_theta_photon);
  checkHistAndWrite2D(h_enery_v_mom_delta_theta_pion);
  checkHistAndWrite2D(h_enery_v_mom_delta_theta_elec);
  checkHistAndWrite2D(h_enery_v_mom_delta_theta_muon);

  checkHistAndWrite2D(h_enery_v_delta_phi_proton);
  checkHistAndWrite2D(h_enery_v_delta_phi_photon);
  checkHistAndWrite2D(h_enery_v_delta_phi_pion);
  checkHistAndWrite2D(h_enery_v_delta_phi_elec);
  checkHistAndWrite2D(h_enery_v_delta_phi_muon);

  checkHistAndWrite2D(h_enery_v_mom_delta_phi_proton);
  checkHistAndWrite2D(h_enery_v_mom_delta_phi_photon);
  checkHistAndWrite2D(h_enery_v_mom_delta_phi_pion);
  checkHistAndWrite2D(h_enery_v_mom_delta_phi_elec);
  checkHistAndWrite2D(h_enery_v_mom_delta_phi_muon);

  checkHistAndWrite2D(h_r_z_muonEntryLayer);

  h_inputPt_v_eta_v_punchThroProb.Write();
  h_caloNormal.Write();
  //checkHistAndWrite1D(h_deltaThetaPion_v_eta_v_punchThroProb);
  //checkHistAndWrite1D(h_deltaPhiPion_v_eta_v_punchThroProb);
  //checkHistAndWrite1D(h_momDeltaThetaPion_v_eta_v_punchThroProb);
  //checkHistAndWrite1D(h_momDeltaPhiPion_v_eta_v_punchThroProb);

  tOutFile->Write();
  tOutFile->Close();

  return;

}

//check 1D histogram function and check its integral
void FCSNTUPAnaSkeleton::checkHistAndWrite1D(TH1F hist){

  //if histogram has no integral don't save it
  if(hist.Integral() < 1.0){return;}

  hist.Write();

}

//check 2D histogram function and check its integral
void FCSNTUPAnaSkeleton::checkHistAndWrite2D(TH2F hist){

  //if histogram has no integral don't save it
  if(hist.Integral() < 1.0){return;}

  hist.Write();

}

//set up branches for the specified tree
void FCSNTUPAnaSkeleton::setUpBranches(TTree * tree, std::vector<Float_t> &o_data_vec, std::vector<std::string> vFeatures){

    for(unsigned int i = 0; i < o_data_vec.size(); i++){
        tree->Branch((vFeatures.at(i)).c_str(), &(o_data_vec.at(i)));
    }

}

//divide a vector element value by a float variable 
void FCSNTUPAnaSkeleton::DivideVectorByScalar(std::vector<Float_t> &v, Float_t k){

    for(auto iter = v.begin(); iter != v.end(); ++iter)
    {
        Float_t val = *iter; // gets the correct element
        if(val == 0.0 || k == 0.0){
            *iter = 0.0;
        }
        *iter = val / k; // works
    }

}

