import os
import fnmatch
import re
import shutil
import subprocess
import argparse
import logging
import sys

# set logging level to INFO
logging.basicConfig(level = logging.INFO)

# commandline arguments
parser = argparse.ArgumentParser(description="Submit to condor (FCSNTUP_analysis)\n\
    Does not include flag file options, will run over all events)",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-user",'--user',type=str,
                    help="Lxplus username, needed to copy input files from eos", required=True)    
parser.add_argument("-exePath",'--exePath',type=str, 
                    help="Executable path", required=True)    
parser.add_argument("-inputTree",'--inputTree',type=str, 
                    help="Tree in input files", required=True)    
parser.add_argument("-directory",'--directory',type=str, 
                    help="Path to input files", required=True)    
parser.add_argument("-outputDir",'--outputDir',type=str, 
                    help="Output directory name", required=True) 
parser.add_argument("-isMyschedd",'--isMyschedd', 
                    help="Run myschedd first to select pool",
                    type=bool,default=False)    
parser.add_argument("-minEnergy",'--minEnergy', 
                    help="Min energy in MeV to create param inputs",
                    type=int,default=40000)    
parser.add_argument("-minEta",'--minEta', 
                    help="Min eta (x100) in MeV to create param inputs",
                    type=int,default=0)    
parser.add_argument("-maxEta",'--maxEta', 
                    help="Max eta (x100) in MeV to create param inputs",
                    type=int,default=320)    
parser.add_argument("-PID",'--PID', 
                    help="PID to use, needs to be available in directory",
                    type=int,default=211)   
args = parser.parse_args()
config = vars(args)
logging.info("Parsing command line arguments")
logging.info("Args: " + str(args))
logging.info(args.user);

#options to run jobs
user=args.user #e.g thcarter
exePath=args.exePath # e.g '/<absolute>/<path>/<to>/FCSNTUP_analysis/run/RunFCSNTUPAna'
minEnergy=args.minEnergy #e.g 40000
minEta=args.minEta #e.g 0
maxEta=args.maxEta #e.g 320
PID=args.PID #e.g 211
inputTree=args.inputTree #e.g 'FCS_ParametrizationInput'
directory=args.directory #e.g "/eos/atlas/atlascerngroupdisk/proj-simul/InputSamplesReprocessing2019" 
outputDir=args.outputDir #e.g "sub_out"

#run 'myschedd bump' first to choose best scheduler 
if (args.isMyschedd == True) :
    subprocess.call('myschedd bump'.split())

# Check directory exists
if (os.path.exists(directory)):
    pass #continue next script
    if (os.path.exists(exePath)):
        pass #continue next script
    else:
        sys.exit(logging.error("exePath '" + exePath + "' not found"))
else:
    sys.exit(logging.error("Directory '" + directory + "' not found"))

# Loop directories
for dirName in os.listdir(directory): #loop through input files
    #select only pion samples
    if fnmatch.fnmatch(dirName, '*pid'+str(PID)+'*'):

        #get energy and eta info from input file name
        energy = dirName[dirName.find('_E')+2:dirName.find('_disj')]
        etaRange = dirName[dirName.find('eta')+4:dirName.find('_zv')]
        etaRangeList = re.findall(r'\d+', etaRange)

        #don't sub job if smaller than min energy
        if(int(energy) < minEnergy):
            continue

        #don't sub job if outside eta range
        if(int(etaRangeList[0]) > maxEta or int(etaRangeList[1]) < minEta):
            continue

        #get correct file from sub directory
        for fileName in os.listdir(os.path.join(directory, dirName)):
            if not fnmatch.fnmatch(fileName, '*NTUP_FCS*'):
                continue
            inputFileName = fileName
            inputFilePath = os.path.join(os.path.join(directory, dirName), fileName)
        logging.info('\ninputFilePath: ' + inputFilePath)

        #create submission directory
        subDir = os.getcwd() + "/" + outputDir + "/pid" + str(PID) + "/" + str(energy) + "/" + str(etaRange)
        os.makedirs(subDir, exist_ok=True)
        os.makedirs(subDir+"/output", exist_ok=True)
        os.makedirs(subDir+"/error", exist_ok=True)
        os.makedirs(subDir+"/log", exist_ok=True)

        finsh = open(os.getcwd() + "/sub_template.sh", "rt")
        foutsh = open(subDir + "/sub.sh", "wt")

        outputFilePath = subDir + "/param_E" + str(energy) +"_"+ etaRange + ".root"

        for line in finsh:
            #read replace the string and write to output file
            line = line.replace('USER', user)
            line = line.replace('EXECUTABLE', exePath)
            line = line.replace('SUBDIR', subDir)
            line = line.replace('INFILE', inputFilePath)
            line = line.replace('OUTFILE', outputFilePath)
            line = line.replace('TMPFILE', inputFileName)
            line = line.replace('PID', str(PID))
            line = line.replace('ENERGY', energy)
            line = line.replace('ETAMIN', etaRangeList[1])
            line = line.replace('ETAMAX', etaRangeList[0])
            #logging.info('line: ' + line)
            foutsh.write(line)
        finsh.close()
        foutsh.close()

        finsub = open(os.getcwd() + "/sub_template.sub", "rt")
        foutsub = open(subDir + "/sub.sub", "wt")

        outputFilePath = os.getcwd() + subDir + "/param_E" + str(energy) +"_"+ etaRange + ".root"

        for line in finsub:
            #read replace the string and write to output file
            line = line.replace('SUBDIR', subDir)
            foutsub.write(line)
        finsub.close()
        foutsub.close()

        subCommand = "condor_submit " + subDir + "/sub.sub"

        subprocess.run(subCommand.split())


    else:
        continue
