# FCSNTUP_analysis

Code to analyse Muon Punch Through of a FCS_NTUP sample, creates root files containing histograms used for the muon punch through parameterization

# compile like

	mkdir run && mkdir obj
	make

# run like

	./run/RunFCSNTUPAna -i /<path>/<to>/FCSNTUP_file.root -t FCS_ParametrizationInput -o <outfile> -pdgID <input_particle_PDG> -energy <input_particle_energy> -etaMin <input_sample_min_eta> -etaMax <input_sample_max_eta>

# Condor submission

Scripts are provided to submit a full param to condor. Supply username, executable path and other options in sub_fullParam_condor.py or sub_fullParam_condor_wFlagFile.py

  * sub_fullParam_condor.py does not include flag file options, will run over all events
  * sub_fullParam_condor_wFlagFile.py allows options to pass a muonFlag file to run over only those that pass selection

To see available options for the condor scripts, do:
```
python3 sub_fullParam_condor.py --help
python3 sub_fullParam_condor_wFlagFile.py --help
```

To run condor scripts:

```
	python3 sub_fullParam_condor.py \
	--user "lxplus_username" \
	--exePath "/abs/path/to/exePath" \
	--minEnergy 50 --minEta 0 --maxEta 320 --PID 211 \
	--inputTree "FCS_ParametrizationInput" \
	--directory "/abs/path/to/directory/" \
	--outputDir "sub_out_woFlags"
```

or 

```
	python3 sub_fullParam_condor_wFlagFile.py \
	--user "lxplus_username" \
	--exePath "/abs/path/to/exePath" \
	--minEnergy 50 --minEta 0 --maxEta 320 --PID 211 \
	--flag "hasxAODGhostMuonSegment" --flagBool 1 \
	--inputTree "FCS_ParametrizationInput" \
	--directory "/abs/path/to/directory/" \
	--directoryFlagFile "/abs/path/to/directoryFlagFile/" \
	--outputDir "sub_out_wFlags_hasxAODGhostMuonSegment_1"
```

To select pool in condor, you can also use option ```--isMyschedd True``` which will run ```myschedd bump```.
See also folder ```test/``` for example on condor submission and scripts.