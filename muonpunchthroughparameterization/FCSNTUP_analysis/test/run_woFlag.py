# run woFlag
python3 sub_fullParam_condor.py \
--user "username" \
--exePath "/eos/user/m/username/gitWork/FCS/muonpunchthroughparameterization/FCSNTUP_analysis/run/RunFCSNTUPAna" \
--minEnergy 50 --minEta 0 --maxEta 500 --PID 211 \
--flagBool 0 \
--inputTree "FCS_ParametrizationInput" \
--directory "/eos/atlas/atlascerngroupdisk/proj-simul/AF3_Run3/InputSampleProduction2022_FixPions/" \
--outputDir "../sub_out_woFlags"
