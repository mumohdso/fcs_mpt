# run w and woFlag
python3 sub_fullParam_condor_wFlagFile.py \
--user "mumohdso" \
--exePath "/afs/cern.ch/user/m/mumohdso/private/Desktop/FCS_MPT_Test/muonpunchthroughparameterization/FCSNTUP_analysis/run/RunFCSNTUPAna" \
--minEnergy 50 --minEta 0 --maxEta 500 --PID 211 \
--flag "hasxAODGhostMuonSegment" --flagBool 1 \
--inputTree "FCS_ParametrizationInput" \
--directory "/eos/atlas/atlascerngroupdisk/proj-simul/AF3_Run3/InputSampleProduction2022_FixPions/" \
--directoryFlagFile "/eos/atlas/atlascerngroupdisk/proj-simul/AF3_Run3/InputSampleProduction2022_FixPions_AOD/" \
--outputDir "sub_out_wFlags_hasxAODGhostMuonSegment_1_6"

python3 sub_fullParam_condor_wFlagFile.py \
--user "mumohdso" \
--exePath "/afs/cern.ch/user/m/mumohdso/private/Desktop/FCS_MPT_Test/muonpunchthroughparameterization/FCSNTUP_analysis/run/RunFCSNTUPAna" \
--minEnergy 50 --minEta 0 --maxEta 500 --PID 211 \
--flag "hasxAODGhostMuonSegment" --flagBool 0 \
--inputTree "FCS_ParametrizationInput" \
--directory "/eos/atlas/atlascerngroupdisk/proj-simul/AF3_Run3/InputSampleProduction2022_FixPions/" \
--directoryFlagFile "/eos/atlas/atlascerngroupdisk/proj-simul/AF3_Run3/InputSampleProduction2022_FixPions_AOD/" \
--outputDir "sub_out_woFlags_hasxAODGhostMuonSegment_0_6"
