#!/bin/bash
# Use user CERNBOX as EOS instance
export EOS_MGM_URL=root://eosuser.cern.ch
# stage-in

eos cp -p INFILE /tmp/USER/TMPFILE
eos cp -p FFPATH /tmp/USER/FFNAME

# run the parameterisation
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh ""
#lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"


EXECUTABLE -i /tmp/USER/TMPFILE -t FCS_ParametrizationInput -o OUTFILE -pdgID PID -energy ENERGY -etaMin ETAMIN -etaMax ETAMAX -flagFileName /tmp/USER/FFNAME -flag FLAG -flagBool FBOOL

# for staging out a single file
rm /tmp/USER/TMPFILE
rm /tmp/USER/FFNAME
