#!/bin/bash
# Use user CERNBOX as EOS instance
export EOS_MGM_URL=root://eosATLAS.cern.ch
# stage-in

eos cp -p INFILE /tmp/USER/TMPFILE

# run the parameterisation
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh ""
#lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"


EXECUTABLE -i /tmp/USER/TMPFILE -t FCS_ParametrizationInput -o OUTFILE -pdgID PID -energy ENERGY -etaMin ETAMIN -etaMax ETAMAX

# for staging out a single file
rm /tmp/USER/TMPFILE
